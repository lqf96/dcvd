// Global variable
int global_var = 1;
// Global constant
const char global_const[] = "123456";

// Structure (Should be ignored)
struct ignore_this {
    char member;
};

// Test function 1
int tlnt_function_1(int a, int b) {
    return a+b;
}

// Test function 2
void tlnt_function_2() {
    tlnt_function_1(1, 3);
}

// Namespace
namespace recurse_into_this {
    // Test function 3
    ignore_this tlnt_function_3() {
        ignore_this struct_var;
        struct_var.member = 1;
        return struct_var;
    }
}
