// Generate data dependence test function
#define DCVD_TEST_DDT_STMT(test_id, stmt) \
    void ddt_function_##test_id() { \
        stmt; \
    }

// Structure
struct test_struct {
    int m;
};

// Simple variables
int a = 1, b = 2, c = 3, d = 4, e = 5, f = 6;
// Array
int array[4] = {0, 1, 2, 3};
// Structure member
test_struct struct_var = {1};
// Pointer
test_struct* p1 = &struct_var;
int* p2 = &f;

// Call dependency test function
int ddt_call_test(int arg0, int* arg1, const int* arg2, int& arg3, const int& arg4) {
    return arg0+(*arg1)+(*arg2)+arg3+arg4;
}

// Plain expression
void ddt_function_1() {
    a+b;
}

// Assign expression
void ddt_function_2() {
    c = a+b;
}

// Compound-assign expression
void ddt_function_3() {
    c += a+b;
}

// Assign to array item
void ddt_function_4() {
    array[b] = c;
}

// Member expression
void ddt_function_5() {
    struct_var.m = a;
}

// Indirect member expression
void ddt_function_6() {
    p1->m = 5;
}

// Indirect assign
void ddt_function_7() {
    *p2 = a*b;
}

// Function call
void ddt_function_8() {
    f = ddt_call_test(a, &b, &c, d, e);
}
