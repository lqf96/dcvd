#include <stdio.h>
#include "gadgets-test.hpp"

// Global variables
int g1 = 1;
int g2 = 5;

// Local function call
void gadgets_local_func(int a) {
    if (a==4)
        a += 4;
    else
        a = 3+a*5;

    a += g1;
}

// Forward declaration (should be ignored)
void gadgets_pdg_pprint(int v1);
#ifdef TEST_MACRO_DEF
long gadgets_generate_func_2();
#endif

// Upstream function 1
void gadgets_upstream_1() {
    char test_str[] = "Hello!";

    gadgets_pdg_pprint(test_str[0]);
#ifdef TEST_MACRO_DEF
    g1 += 1;
    gadgets_generate_func_2();
#endif
}

// Upstream function 2
void gadgets_upstream_2() {
    int test = g1+g2;

    gadgets_pdg_pprint(test);
}

// PDG generation and pretty-print test
void gadgets_pdg_pprint(int v1) {
    bool v2 = true;
    int v3 = v1+(v2?3:5);
    int v4 = g1;
    unsigned int v5 = 11;
    char str[] = "Hello world.";

    for (int i=0;i<v1;i++) {
        v4 *= i;
    }
    str[v5] = '!';
    g2 += v4;

    gadgets_vul_func_1(&v3);
    gadgets_non_vul_func(&v3);
    gadgets_vul_func_2(v1);

    g2 = 7;

    gadgets_local_func(v3);

    int v6 = 8;

    v6 += v3-4;
    v3 -= 1;
    v3 = v3+2;
}

#ifdef TEST_MACRO_DEF
// Gadgets generation test function 2
long gadgets_generate_func_2() {
    bool v2 = false;
    int v5 = g1;

    gadgets_ignore_func();
    gadgets_vul_func_2(v2?3000:5000);

    return v5;
}
#endif
