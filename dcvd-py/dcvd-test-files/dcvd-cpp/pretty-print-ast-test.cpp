int test_pretty_print_ast(int arg0, char* arg1) {
    // Variable declaration
    int a = 0;
    char b[] = "123";
    float c = 1.2345;
    bool d = false;
    double e, f = 2;
    unsigned g[] = {1, 2, 4, 8};

    // If statement
    if (d) {
        a += 1;
    } else {
        b[a] = '1';
    }

    // For statement
    for (unsigned int i = 0;i<sizeof(b);i++) {
        b[i] += 1;

        // While statement
        while (d) {
            test_pretty_print_ast(a, b);
            c++;
        }
    }

    // Do statement
    do {
        a = a*a;
    } while (d);

    // Try-catch statement
    try {
        throw 1;
    } catch (int r) {
        a %= r;
    } catch (...) {
        return 5;
    }

    return 2;
}
