// Null pointer
#define nullptr 0

// Global array
int gadgets_data[] = {2, 4, 6, 8};

// Vulnerable function call 1
int gadgets_vul_func_1(int* p) {
    return *p;
}

// Vulnerable function call 2
int gadgets_vul_func_2(int idx) {
    gadgets_data[idx] += 2;
    return 0;
}

// Non-vulnerable function call
int gadgets_non_vul_func(int* p) {
    if (p!=nullptr)
        return *p;
    else
        return 0;
}

// Function that should be ignored
void gadgets_ignore_func();
