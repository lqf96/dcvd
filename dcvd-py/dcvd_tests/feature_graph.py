from unittest import TestCase
from networkx import DiGraph

from dcvd_cpp import parse_testcase_source
from dcvd.cpg.ast import dcvd_cpp_args, pretty_print_ast
from dcvd.cpg.pdg import PDGConstructor, CallsCollector, pretty_print_cpg, \
    pretty_print_pdg, pretty_print_cpg, top_level_names
from dcvd.cpg.feature_graph import SliceBuilder, FeatureGraphBuilder, \
    gadget_graphs_from_source
from dcvd.cpg import consts as cpg_consts
from .util import test_file_path, print_separator

class CXXFeatureGraphTestCase(TestCase):
    """ C/C++ feature graph testcase. """
    def test_00_generate_slice(self):
        """ Test slice generation, both upstream and downstream. """
        # Prepare CLI arguments
        cli_args = dcvd_cpp_args([
            test_file_path("dcvd-cpp/gadgets-test.cpp")
        ])
        # Parse AST
        ast_cpgs = parse_testcase_source(cli_args)
        # Get top-level names
        local_func_names, global_var_names = top_level_names(ast_cpgs)
        func_cpg_id = local_func_names["gadgets_pdg_pprint"]
        func_cpg = ast_cpgs[func_cpg_id]
        # Build PDG
        pdg_constructor = PDGConstructor(global_var_names)
        pdg_constructor.construct_pdg(func_cpg)
        # Collect function call information
        calls_collector = CallsCollector(
            local_func_names,
            vul_ext_func_names=["gadgets_vul_func_1"]
        )
        calls_collector.collect_calls(func_cpg)
        entrypoint = calls_collector.vul_ext_func_calls["gadgets_vul_func_1"][0]
        # Slice graph
        slice_graph_upstream = DiGraph()
        slice_graph_downstream = DiGraph()
        # Build upstream slice
        n_upstream_slice = SliceBuilder().build_slice(
            source_cpg=func_cpg,
            cpg_id=func_cpg_id,
            target_cpg=slice_graph_upstream,
            entrypoint=entrypoint,
            upstream=True,
            stmt_limit=10
        )
        # Build downstream slice
        n_downstream_slice = SliceBuilder().build_slice(
            source_cpg=func_cpg,
            cpg_id=func_cpg_id,
            target_cpg=slice_graph_downstream,
            entrypoint=entrypoint,
            upstream=False,
            stmt_limit=5
        )
        # Pretty-print copied statement AST
        with print_separator("Statement AST Copy"):
            pretty_print_ast(func_cpg, entrypoint)
        # Pretty-print upstream slice graph
        with print_separator("Upstream Slice Graph"):
            print("Upstream slice size: {}".format(n_upstream_slice))
            pretty_print_pdg(slice_graph_upstream)
        # Pretty-print downstream slice graph
        with print_separator("Downstream Slice Graph"):
            print("Downstream slice size: {}".format(n_downstream_slice))
            pretty_print_pdg(slice_graph_downstream)
        # Upstream and downstream function AST and CPG
        upstream_func_1_cpg = ast_cpgs[local_func_names["gadgets_upstream_1"]]
        upstream_func_2_cpg = ast_cpgs[local_func_names["gadgets_upstream_2"]]
        downstream_func_cpg = ast_cpgs[local_func_names["gadgets_local_func"]]
        # Build PDG for upstream and downstream functions
        pdg_constructor.construct_pdg(upstream_func_1_cpg)
        pdg_constructor.construct_pdg(upstream_func_2_cpg)
        pdg_constructor.construct_pdg(downstream_func_cpg)
        # Pretty-print AST and CPG for these functions
        with print_separator("AST for Upstream Function 1"):
            pretty_print_ast(upstream_func_1_cpg)
        with print_separator("PDG for Upstream Function 1"):
            pretty_print_pdg(upstream_func_1_cpg)
        with print_separator("AST for Upstream Function 2"):
            pretty_print_ast(upstream_func_2_cpg)
        with print_separator("PDG for Upstream Function 2"):
            pretty_print_pdg(upstream_func_2_cpg)
        with print_separator("AST for Downstream Function"):
            pretty_print_ast(downstream_func_cpg)
        with print_separator("PDG for Downstream Function"):
            pretty_print_pdg(downstream_func_cpg)
        # Print global variable to CPG ID mapping
        with print_separator("Global Variables"):
            print(global_var_names)
    def test_01_generate_gadget_graph(self):
        """ Test gadget graph generation. """
        # Generate gadget graphs
        gadget_graphs = gadget_graphs_from_source(
            testcase_sources=[
                test_file_path("dcvd-cpp/gadgets-test.cpp")
            ],
            macro_defs=[],
            local_include_paths=[],
            source_ignore_paths=[],
            vul_func_names=["gadgets_vul_func_1"],
            upstream_limit=10,
            downstream_limit=8
        )
        # Only 1 graph should be generated
        self.assertEqual(len(gadget_graphs), 1)
        # Pretty-print gadget graph
        with print_separator("Gadget Graph"):
            pretty_print_cpg(
                gadget_graphs[0],
                [cpg_consts.PDG_EDGE, cpg_consts.GADGET_LINK_EDGE]
            )
    def test_02_gadget_graphs_for_mock_testcase(self):
        """ Generate gadget graph for mock testcase. """
        # Generate gadget graphs
        gadget_graphs = gadget_graphs_from_source(
            testcase_sources=[
                test_file_path("dcvd-cpp/gadgets-test.cpp"),
                test_file_path("dcvd-cpp/gadgets-test-ignore.cpp")
            ],
            macro_defs=["TEST_MACRO_DEF"],
            source_ignore_paths=[
                test_file_path("dcvd-cpp/gadgets-test-ignore.cpp")
            ],
            local_include_paths=[],
            vul_func_names=[
                "gadgets_vul_func_1",
                "gadgets_vul_func_2"
            ],
            upstream_limit=10,
            downstream_limit=8
        )
        # 3 graphs should be generated
        self.assertEqual(len(gadget_graphs), 3)
        # Print the entrypoints of gadget graphs
        with print_separator("Gadget Graph Entrypoints"):
            print([cpg.graph["entrypoint"] for cpg in gadget_graphs])
    def test_03_feature_graphs_for_mock_testcase(self):
        """ Test feature graph generation. """
        # Generate gadget graphs
        gadget_graphs = gadget_graphs_from_source(
            testcase_sources=[
                test_file_path("dcvd-cpp/gadgets-test.cpp"),
                test_file_path("dcvd-cpp/gadgets-test-ignore.cpp")
            ],
            macro_defs=["TEST_MACRO_DEF"],
            source_ignore_paths=[
                test_file_path("dcvd-cpp/gadgets-test-ignore.cpp")
            ],
            local_include_paths=[],
            vul_func_names=[
                "gadgets_vul_func_1",
                "gadgets_vul_func_2"
            ],
            upstream_limit=10,
            downstream_limit=8
        )
        # Feature graph builder
        builder = FeatureGraphBuilder()
        # Generate raw feature for graphs
        # (Training mode only for graph 1)
        for i, gadget_graph in enumerate(gadget_graphs):
            builder.gen_raw_feature(
                gadget_graph,
                gadget_graph.graph["entrypoint"],
                training=(i==0)
            )
        builder.build_rev_tag_map()
        # Generate feature graphs
        feature_graphs = [
            builder.gen_feature_graph(
                gadget_graph,
                gadget_graph.graph["entrypoint"]
            ) for gadget_graph in gadget_graphs
        ]
        # Pretty-print the first gadget graph
        with print_separator("Gadget Graph"):
            pretty_print_cpg(
                gadget_graphs[0],
                [cpg_consts.PDG_EDGE, cpg_consts.GADGET_LINK_EDGE]
            )
        # Pretty-print the first feature graph
        with print_separator("Feature Graph"):
            first_feature_graph = feature_graphs[0]
            for node_id in first_feature_graph.nodes:
                successors = first_feature_graph.successors(node_id)
                if successors:
                    print("{}: {}".format(node_id, list(successors)))
