from unittest import TestSuite, makeSuite

from .ast import CXXASTTestCase
from .pdg import CXXPDGTestCase
from .feature_graph import CXXFeatureGraphTestCase
from .sard_testcase import CXXSARDTestCaseA

# Create test suite
test_suite = TestSuite([
    makeSuite(CXXASTTestCase),
    makeSuite(CXXPDGTestCase),
    makeSuite(CXXFeatureGraphTestCase),
    makeSuite(CXXSARDTestCaseA)
])
