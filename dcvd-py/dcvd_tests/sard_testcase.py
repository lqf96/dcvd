import json
from io import StringIO
from unittest import TestCase
from networkx import convert_node_labels_to_integers

from dcvd_cpp import parse_testcase_source
from dcvd.cpg.ast import pretty_print_ast, dcvd_cpp_args
from dcvd.cpg.pdg import PDGConstructor, CallsCollector, pretty_print_pdg, \
    top_level_names, pretty_print_cpg
from dcvd.cpg.feature_graph import FeatureGraphBuilder, gadget_graphs_from_source
from dcvd.legacy.dgcnn_io import write_feature_graphs
from dcvd.cpg import consts as cpg_consts
from .util import test_file_path, sard_file_path, feature_graph_node_repr, \
    print_separator

class CXXSARDTestCaseBase(TestCase):
    """ Test feature graph generation on SARD testcase #64232. """
    # Source ignore paths
    SOURCE_IGNORE_PATHS = [
        sard_file_path("shared")
    ]
    # Local include paths
    LOCAL_INCLUDE_PATHS = [
        sard_file_path("shared/86")
    ]
    # Vulnerable function names
    with open(test_file_path("vdp-vul-func-names.json")) as f:
        VDP_VUL_FUNC_NAMES = json.load(f)
    @classmethod
    def setUpClass(cls):
        """ Set-up testcase. """
        # Prepare CLI arguments for testcase
        cli_args = dcvd_cpp_args(
            source_files=cls.TESTCASE_SOURCES,
            source_ignore_paths=cls.SOURCE_IGNORE_PATHS,
            macro_defs=["OMITBAD"],
            local_include_paths=cls.LOCAL_INCLUDE_PATHS
        )
        # Parse AST
        cpgs = cls.cpgs = parse_testcase_source(cli_args)
        # Get top-level names
        local_func_names, global_var_names = top_level_names(cpgs)
        cls.local_func_names = local_func_names
        cls.global_var_names = global_var_names
        # Local function calls reverse mapping
        local_call_rev_map = {}
        # Build PDG for each function
        pdg_constructor_map = {}
        calls_collector_map = {}
        for func_name, i in local_func_names.items():
            func_cpg = cpgs[i]
            # Build PDG and append PDG constructor
            pdg_constructor = PDGConstructor(global_var_names)
            pdg_constructor.construct_pdg(func_cpg)
            pdg_constructor_map[i] = pdg_constructor
            # Collect calls and append calls collector
            calls_collector = CallsCollector(
                local_func_names,
                vul_ext_func_names=cls.VDP_VUL_FUNC_NAMES["CWE-119"],
            )
            calls_collector.collect_calls(func_cpg)
            calls_collector_map[i] = calls_collector
            # Update local function calls reverse mapping
            for local_func_name, node_ids in calls_collector.local_func_calls.items():
                call_rev_map = local_call_rev_map.setdefault(local_func_name, {})
                call_rev_map[func_name] = node_ids
        # Build good gadget graphs
        gadget_graphs = cls.gadget_graphs = []
        gadget_graphs += gadget_graphs_from_source(
            testcase_sources=cls.TESTCASE_SOURCES,
            macro_defs=["OMITGOOD"],
            source_ignore_paths=cls.SOURCE_IGNORE_PATHS,
            local_include_paths=cls.LOCAL_INCLUDE_PATHS,
            vul_func_names=cls.VDP_VUL_FUNC_NAMES["CWE-119"],
            upstream_limit=40,
            downstream_limit=40
        )
        gadget_graphs += gadget_graphs_from_source(
            testcase_sources=cls.TESTCASE_SOURCES,
            macro_defs=["OMITBAD"],
            source_ignore_paths=cls.SOURCE_IGNORE_PATHS,
            local_include_paths=cls.LOCAL_INCLUDE_PATHS,
            vul_func_names=cls.VDP_VUL_FUNC_NAMES["CWE-119"],
            upstream_limit=40,
            downstream_limit=40
        )
        # Feature graph builder
        builder = FeatureGraphBuilder()
        # Generate raw feature for graphs
        # (Training mode only for graph 1)
        for i, gadget_graph in enumerate(gadget_graphs):
            builder.gen_raw_feature(
                gadget_graph,
                gadget_graph.graph["entrypoint"],
                training=True
            )
        builder.build_rev_tag_map()
        # Generate feature graphs
        cls.feature_graphs = [
            builder.gen_feature_graph(
                gadget_graph,
                gadget_graph.graph["entrypoint"]
            ) for gadget_graph in gadget_graphs
        ]
    def test_00_print_names(self):
        """ Print global variable and local function names. """
        with print_separator("Global Variables"):
            print(self.global_var_names)
        with print_separator("Local Functions"):
            print(self.local_func_names)
    def test_01_print_ast(self):
        """ Print AST of generated CPGs. """
        for func_name, cpg_id in self.local_func_names.items():
            cpg = self.cpgs[cpg_id]
            with print_separator("AST for {}".format(func_name)):
                pretty_print_ast(cpg)
    def test_02_print_pdg(self):
        """ Print PDG of generated CPGs. """
        for func_name, cpg_id in self.local_func_names.items():
            cpg = self.cpgs[cpg_id]
            with print_separator("PDG for {}".format(func_name)):
                pretty_print_pdg(cpg)
    def test_03_print_gadget_graph(self):
        """ Print gadget graphs generated from CPGs. """
        for i, gadget_graph in enumerate(self.gadget_graphs):
            with print_separator("Gadget Graph {}".format(i+1)):
                print("Entrypoint: {}".format(gadget_graph.graph["entrypoint"]))
                pretty_print_cpg(
                    gadget_graph,
                    [cpg_consts.PDG_EDGE, cpg_consts.GADGET_LINK_EDGE]
                )
    def test_04_print_feature_graph(self):
        """ Print final feature graphs generated from gadget graphs. """
        for i, feature_graph in enumerate(self.feature_graphs):
            # Print raw feature graph
            with print_separator("Raw Feature Graph {}".format(i+1)):
                for node_id in feature_graph.nodes:
                    successors = feature_graph.successors(node_id)
                    if successors:
                        successors_info = ", ".join((
                            feature_graph_node_repr(feature_graph, successor_id)
                            for successor_id in successors
                        ))
                        print("{}: [{}]".format(node_id, successors_info))
            # Print renumbered graph
            with print_separator("Renumbered Feature Graph {}".format(i+1)):
                feature_graph = convert_node_labels_to_integers(feature_graph)
                # Write feature graph to file
                f = StringIO()
                write_feature_graphs(f, [feature_graph], [0])
                # Show renumbered graph
                print(f.getvalue())

class CXXSARDTestCaseA(CXXSARDTestCaseBase):
    """ Test feature graph generation for SARD testcase #64232. """
    # Testcase sources
    TESTCASE_SOURCES = [
        sard_file_path("000/064/232/CWE121_Stack_Based_Buffer_Overflow__CWE805_char_declare_ncpy_01.c"),
        # Shared files
        sard_file_path("shared/86/io.c"),
        sard_file_path("shared/86/std_thread.c")
    ]
