import os
from unittest import TestCase

from dcvd_cpp import parse_testcase_source
from dcvd.cpg.ast import dcvd_cpp_args, pretty_print_ast
from dcvd.cpg.pdg import DataDependencyContext, PDGConstructor, CallsCollector, \
    top_level_names, pretty_print_pdg
from .util import test_file_path, print_separator

class CXXPDGTestCase(TestCase):
    """ C/C++ PDG testcase. """
    def test_00_top_level_names(self):
        """ Test top-level names of source file. """
        # Prepare CLI arguments
        cli_args = dcvd_cpp_args([
            test_file_path("dcvd-cpp/top-level-names-test.cpp")
        ])
        # Parse AST
        ast_cpgs = parse_testcase_source(cli_args)
        # Get top-level names
        local_func_names, global_var_names = top_level_names(ast_cpgs)
        # Print top-level names
        with print_separator("Top-level Names"):
            print("Local function names: {}".format(", ".join(local_func_names.keys())))
            print("Global variables: {}".format(", ".join(global_var_names.keys())))
    ## Expected statement-level data dependency result
    _DDT_EXPECTED_RESULT = [
        # Plain expression
        (["a", "b"], []),
        # Assign expression
        (["a", "b"], ["c"]),
        # Compound-assign expression
        (["a", "b", "c"], ["c"]),
        # Assign to array item
        (["array", "b", "c"], ["array"]),
        # Member expression
        (["struct_var", "a"], ["struct_var"]),
        # Indirect member expression
        (["p1"], []),
        # Indirect assign
        (["p2", "a", "b"], []),
        # Function call
        (["a", "b", "c", "d", "e", "ddt_call_test"], ["b", "d", "f"])
    ]
    def test_01_data_dependence(self):
        """ Test statement-level data dependency. """
        # Prepare CLI arguments
        cli_args = dcvd_cpp_args([
            test_file_path("dcvd-cpp/data-dependence-test.cpp")
        ])
        # Parse AST
        ast_cpgs = parse_testcase_source(cli_args)
        # Get top-level names
        local_func_names, _ = top_level_names(ast_cpgs)
        with print_separator("Local Functions"):
            print(local_func_names)
        # DDT function name template
        DDT_TMPL = "ddt_function_{}"
        # Assert analyzation result
        for i, (expected_in_vars, expected_out_vars) in enumerate(self._DDT_EXPECTED_RESULT):
            # Get CPG for function
            cpg_index = local_func_names[DDT_TMPL.format(i+1)]
            cpg = ast_cpgs[cpg_index]
            # Analyze statement-level data dependency
            in_vars, out_vars = DataDependencyContext().analyze(cpg, 2)
            # Assert result
            self.assertEqual(
                set(expected_in_vars),
                in_vars,
                "In variables for function {} don't match".format(i+1)
            )
            self.assertEqual(
                set(expected_out_vars),
                out_vars,
                "Out variables for function {} don't match".format(i+1)
            )
    def test_02_pdg_generation(self):
        """ Test PDG generation on existing AST. """
        # Prepare CLI arguments
        cli_args = dcvd_cpp_args([
            test_file_path("dcvd-cpp/gadgets-test.cpp")
        ])
        # Parse AST and get top-level names
        ast_cpgs = parse_testcase_source(cli_args)
        local_func_names, global_var_names = top_level_names(ast_cpgs)
        # Find CPG for function
        cpg = ast_cpgs[local_func_names["gadgets_pdg_pprint"]]
        # Generate PDG on CPG
        pdg_constructor = PDGConstructor(global_var_names)
        pdg_constructor.construct_pdg(cpg)
        # Pretty print AST and PDG
        with print_separator("AST"):
            pretty_print_ast(cpg)
        with print_separator("PDG"):
            pretty_print_pdg(cpg)
        # Assert global variable reads and writes
        with print_separator("Global Variable Reads"):
            print(pdg_constructor.global_var_reads)
            self.assertEqual(
                pdg_constructor.global_var_reads.keys(),
                set(["g1", "g2"])
            )
        with print_separator("Global Variable Writes"):
            print(pdg_constructor.global_var_writes)
            self.assertEqual(
                pdg_constructor.global_var_writes.keys(),
                set(["g2"])
            )
    def test_03_calls_collector(self):
        """ Test function calls collector. """
        # Prepare CLI arguments
        cli_args = dcvd_cpp_args([
            test_file_path("dcvd-cpp/gadgets-test.cpp")
        ])
        # Parse AST and get top-level names
        ast_cpgs = parse_testcase_source(cli_args)
        local_func_names, global_var_names = top_level_names(ast_cpgs)
        # Find CPG for function
        cpg = ast_cpgs[local_func_names["gadgets_pdg_pprint"]]
        # Collect function call information
        calls_collector = CallsCollector(
            local_func_names,
            vul_ext_func_names=[
                "gadgets_vul_func_1",
                "gadgets_vul_func_2"
            ]
        )
        calls_collector.collect_calls(cpg)
        # Assert local function calls
        with print_separator("Local Function Calls"):
            print(calls_collector.local_func_calls)
            self.assertEqual(
                calls_collector.local_func_calls.keys(),
                set(["gadgets_local_func"])
            )
        # Print vulnerable call locations
        with print_separator("Vulnerable Calls"):
            print(calls_collector.vul_ext_func_calls)
