import os
from contextlib import contextmanager
from collections import Iterable

# Tests package directory
TESTS_PKG_DIR = os.path.basename(__file__)

def test_file_path(file_path, test_file_dir="../dcvd-test-files"):
    """ Generate absolute file paths for test file. """
    # Relative path to file
    relative_path = os.path.join(TESTS_PKG_DIR, test_file_dir, file_path)
    # Convert to absolute path
    return os.path.abspath(relative_path)

def sard_file_path(file_path, sard_testcases_dir="../../local/sard-dataset/testcases"):
    # Relative path to file
    relative_path = os.path.join(TESTS_PKG_DIR, sard_testcases_dir, file_path)
    # Convert to absolute path
    return os.path.abspath(relative_path)

def feature_graph_node_repr(cpg, node_id):
    """ Return the representation of feature graph node. """
    node_info = cpg.nodes[node_id]
    # Node ID, raw tag and tag
    repr_str = "{}: {}".format(node_id, node_info.get("tag"))
    raw_tag = node_info.get("raw_tag")
    if raw_tag!=None:
        repr_str += " [Raw: {}]".format(raw_tag)
    return repr_str

@contextmanager
def print_separator(begin_title, end_title="End"):
    print("\n========== {} ==========".format(begin_title))
    yield
    print("========== {} ==========".format(end_title))
