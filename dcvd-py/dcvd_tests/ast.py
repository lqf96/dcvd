from unittest import TestCase

from dcvd_cpp import parse_testcase_source
from dcvd.cpg.ast import dcvd_cpp_args, pretty_print_ast
from .util import test_file_path, print_separator

class CXXASTTestCase(TestCase):
    """ C/C++ AST testcase. """
    def test_00_pretty_print_ast(self):
        """ Test pretty print AST. """
        # Prepare CLI arguments
        cli_args = dcvd_cpp_args([
            test_file_path("dcvd-cpp/pretty-print-ast-test.cpp")
        ])
        # Parse AST
        ast_cpg = parse_testcase_source(cli_args)[0]
        # Pretty print AST
        with print_separator("AST"):
            pretty_print_ast(ast_cpg)
