#! /usr/bin/env python3
from setuptools import setup

setup(
    name="dcvd",
    version="0.1.0",
    descriptor="Deep code vulnerability detector",
    author="Qifan Lu",
    author_email="lqf96@uw.edu",
    url="https://github.com/lqf96/dcvd",
    packages=["dcvd"],
    py_modules=["dcvd_cpp"],
    install_requires=[
        "numpy",
        "networkx"
    ],
    test_suite="dcvd_tests.test_suite"
)