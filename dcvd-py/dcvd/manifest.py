import re, logging
from xml.etree import ElementTree

# Logger
_logger = logging.getLogger(__name__)

# Good, bad or mixed element tags
GOOD_BAD_MIXED_TAGS = set(["flaw", "fix", "mixed"])
# Vulnerability regular expression
VUL_REGEX = re.compile(r"^(CWE\-\d+): (.*)$")

def parse_testcase_info(testcase_node):
    """ Parse <testcase> element in manifest. """
    # Testcase ID, language and flaw type
    id = int(testcase_node.attrib["id"])
    lang = testcase_node.attrib["language"]
    testsuite_id = testcase_node.attrib.get("testsuiteid")
    flaw_type = None
    # Parse testsuite ID
    if testsuite_id:
        try:
            testsuite_id = int(testsuite_id.split(" ")[0])
        except:
            testsuite_id = None
    # Source files
    source_files = []
    for child in testcase_node:
        # <file> element only
        if child.tag!="file":
            continue
        # Source path and flaw lines
        source_path = child.attrib["path"]
        flaw_lines = []
        # Flaw lines
        for grandchild in child:
            # <flaw>, <fix> or <mixed> elements only
            tag_name = grandchild.tag
            if tag_name not in GOOD_BAD_MIXED_TAGS:
                continue
            flaw_type = tag_name
            # Line number
            line_number = int(grandchild.attrib["line"])
            # Vulnerability category and description
            vul_match = VUL_REGEX.match(grandchild.attrib["name"])
            if vul_match:
                vul_category, vul_description = vul_match.groups()
            else:
                vul_category = vul_description = None
            # Flaw lines
            flaw_lines.append({
                "type": tag_name,
                "line": line_number,
                "vul_category": vul_category,
                "vul_description": vul_description
            })
        # Append file
        source_files.append({
            "path": source_path,
            "flaw_lines": flaw_lines
        })
    # Testcase information
    return {
        "id": id,
        "lang": lang,
        "testsuite_id": testsuite_id,
        "flaw_type": flaw_type,
        "source_files": source_files
    }

def parse_testcase_manifest(manifest_path):
    """ Parse testcase manifest file and return testcase information. """
    # Read and parse manifest sequentially
    for event, element in ElementTree.iterparse(manifest_path, ["end"]):
        # Parse testcase information
        if element.tag=="testcase":
            yield parse_testcase_info(element)
            element.clear()
