import re

from dcvd_cpp import get_kind_name, get_kind_repr
from .cpg import BRANCH_AST_KINDS, traverse_ast, statement_only_visitor, \
    ast_kinds_visitor, ast_children, cpg_neighbors
from . import consts

def top_level_names(cpgs):
    """ Get top-level function and global variable names. """
    # Top-level function and global variable names
    local_func_names = {}
    global_var_names = {}
    # Process each CPG
    for i, cpg in enumerate(cpgs):
        root_info = cpg.nodes[(consts.AST_NODE, cpg.graph["root"])]
        decl_kind = root_info["decl_kind"]
        decl_kind_name = get_kind_name("decl_kind", decl_kind)
        # Global variable
        if decl_kind_name=="Var":
            var_name = root_info["name"]
            global_var_names[var_name] = i
        # Function
        elif decl_kind_name=="Function":
            func_name = root_info["name"]
            local_func_names[func_name] = i
    return local_func_names, global_var_names

# Function call regular expression
FUNC_CALL_REGEX = re.compile(r"^.*\(\*\)\((.*)\)$")

def callee_params_in_out(func_type):
    """ Get whether parameters of a function call is 'in' or 'in-out'. """
    in_out_params = []
    # Get parameter types
    param_types_match = FUNC_CALL_REGEX.match(func_type)
    if not param_types_match:
        return None
    param_types_raw = param_types_match.group(1)
    # Function itself is "in" only
    in_out_params = [False]
    # No parameters
    if param_types_raw=="" or param_types_raw=="void":
        return in_out_params
    param_types = param_types_raw.split(", ")
    # Analyze each parameter type
    for param_type in param_types:
        # Mutable pointer or reference type: "in-out"
        if param_type.endswith(("&", "*")) and not param_type.startswith("const "):
            in_out_params.append(True)
        # Others are "in" parameters
        else:
            in_out_params.append(False)
    return in_out_params

class DataDependencyContext(object):
    """ Data dependency context class. """
    def __init__(self):
        """ Initialize data dependence context. """
        ## In variables
        self._in_vars = set()
        ## Out variables
        self._out_vars = set()
        ## In-out marker stack
        self._in_out_stack = []
    def _stack_top(self):
        """ Get in-out stack top item. """
        if self._in_out_stack:
            node_id, _in, out = self._in_out_stack[-1]
        else:
            _in = True
            out = False
        return _in, out
    def _handle_decl(self, cpg, node_info, ast_node_id, depth):
        """ Handle declaration AST nodes. """
        # Variable or parameter declaration only
        decl_kind = node_info["decl_kind"]
        decl_kind_name = get_kind_name("decl_kind", decl_kind)
        # Get variable declarations
        if decl_kind_name=="Var":
            decls = node_info["decls"]
        elif decl_kind_name=="Function":
            decls = node_info["param_decls"]
        else:
            return True
        # Add to out variables
        for var_name, _ in decls:
            self._out_vars.add(var_name)
        # Mark branch as "in"
        children = ast_children(cpg, ast_node_id)
        for child_id in reversed(children):
            self._in_out_stack.append((child_id, True, False))
        return True
    def _handle_unary_op(self, cpg, node_info, ast_node_id, depth):
        """ Handle unary operator AST nodes. """
        unary_op_kind = node_info["unary_op_kind"]
        unary_op_repr = get_kind_repr("unary_op_kind", unary_op_kind)
        # Only child
        child_id, = ast_children(cpg, ast_node_id)
        # In-out marker of the subtree
        _in, out = self._stack_top()
        # Deref operator
        if unary_op_repr=="*" and out:
            # Only branch marked as "in"
            self._in_out_stack.append((child_id, True, False))
        return True
    def _handle_binary_op(self, cpg, node_info, ast_node_id, depth):
        """ Handle binary operator AST nodes. """
        binary_op_kind = node_info["binary_op_kind"]
        binary_op_repr = get_kind_repr("binary_op_kind", binary_op_kind)
        # Left and right children
        left_id, right_id = ast_children(cpg, ast_node_id)
        # Assign operator
        if binary_op_repr=="=":
            # Right branch marked as "in"
            self._in_out_stack.append((right_id, True, False))
            # Left branch marked as "out"
            self._in_out_stack.append((left_id, False, True))
        return True
    def _handle_compound_assign_op(self, cpg, node_info, ast_node_id, depth):
        """ Handle compound assign operator AST nodes. """
        left_id, right_id = ast_children(cpg, ast_node_id)
        # Right branch marked as "in"
        self._in_out_stack.append((right_id, True, False))
        # Left branch marked as "in-out"
        self._in_out_stack.append((left_id, True, True))
        return True
    def _handle_array_subscript_expr(self, cpg, node_info, ast_node_id, depth):
        """ Handle array subscript expression AST nodes. """
        left_id, right_id = ast_children(cpg, ast_node_id)
        # In-out marker of the subtree
        _in, out = self._stack_top()
        if out:
            # Right branch marked as "in"
            self._in_out_stack.append((right_id, True, False))
            # Left branch mark as "in-out"
            self._in_out_stack.append((left_id, True, True))
        return True
    def _handle_member_expr(self, cpg, node_info, ast_node_id, depth):
        """ Handle member expression AST nodes. """
        child_id, = ast_children(cpg, ast_node_id)
        # In-out marker of the subtree
        _in, out = self._stack_top()
        if out:
            # Indirect accrss, mark branch as "in" only
            if node_info["indirect"]:
                self._in_out_stack.append((child_id, True, False))
            # Direct access, mark branch as "in-out"
            else:
                self._in_out_stack.append((child_id, True, True))
        return True
    def _handle_call_expr(self, cpg, node_info, ast_node_id, depth):
        """ Handle function call expression AST nodes. """
        children = ast_children(cpg, ast_node_id)
        # Analyze callee type for in-out information
        callee_id = (consts.AST_NODE, children[0])
        in_out_params = callee_params_in_out(cpg.nodes[callee_id]["type"])
        # Fallback if function type parsing failed
        if in_out_params==None:
            in_out_params = [False]*len(children)
        # Mark each branch as "in" or "in-out"
        for child_id, out in zip(reversed(children), reversed(in_out_params)):
            self._in_out_stack.append((child_id, True, out))
        return True
    def _handle_decl_ref_expr(self, cpg, node_info, ast_node_id, depth):
        """ Handle declaration reference expression AST nodes. """
        # In-out marker of the subtree
        _in, out = self._stack_top()
        # Add variable to in or out variables
        var_name = node_info["name"]
        if _in:
            self._in_vars.add(var_name)
        if out:
            self._out_vars.add(var_name)
        return True
    def _on_exit(self, cpg, node_info, ast_node_id, depth):
        """ Callback when exiting an AST node. """
        if not self._in_out_stack:
            return
        # Pop node ID from in-out stack
        top_node_id = self._in_out_stack[-1][0]
        if top_node_id==ast_node_id:
            self._in_out_stack.pop()
    def analyze(self, cpg, ast_node_id):
        """ Analyze the data dependence of given statement. """
        # Traverse statement AST and build data dependency
        traverse_ast(
            cpg,
            ast_kinds_visitor({
                "UnaryOperator": self._handle_unary_op,
                "BinaryOperator": self._handle_binary_op,
                "CompoundAssignOperator": self._handle_compound_assign_op,
                "CallExpr": self._handle_call_expr,
                "DeclRefExpr": self._handle_decl_ref_expr,
                "ArraySubscriptExpr": self._handle_array_subscript_expr,
                "MemberExpr": self._handle_member_expr,
            }),
            self._on_exit,
            ast_node_id
        )
        return self._in_vars, self._out_vars

class PDGConstructor(object):
    """ PDG constructor class. """
    def __init__(self, global_var_names):
        """ Initialize PDG constructor. """
        ## Global variable names
        self.global_var_names = set(global_var_names)
        ## Global variable reads
        self.global_var_reads = {}
        ## Global variable writes
        self.global_var_writes = {}
        ## Scope stack
        self._scope_stack = []
        ## Variable reference map
        self._var_refs_map = {}
    def _update_var_ref(self, var_name, ref_id):
        """ Update variable reference. """
        # Get reference stack for variable
        var_refs = self._var_refs_map.get(var_name)
        if var_refs==None:
            return False
        # Update reference
        var_ref_item = (var_refs[-1][0], ref_id)
        var_refs[-1] = var_ref_item
        return True
    def _create_var_ref(self, var_name, scope_id, ref_id):
        """ Create variable reference. """
        # Get reference stack for variable
        var_refs = self._var_refs_map.setdefault(var_name, [])
        # Push reference item into stack
        var_refs.append((scope_id, ref_id))
    def _get_var_ref(self, var_name):
        """ Get variable reference. """
        # Get reference stack for variable
        var_refs = self._var_refs_map.get(var_name)
        if not var_refs:
            return None
        # Return reference ID
        return var_refs[-1][1]
    def _add_pdg_edges(self, cpg, in_vars, out_vars, ast_node_id):
        """ Add PDG edges to graph. """
        # "In" variables
        for in_var in in_vars:
            ref_id = self._get_var_ref(in_var)
            # Local variable
            if ref_id!=None:
                cpg.add_edge(
                    (consts.AST_NODE, ast_node_id),
                    (consts.AST_NODE, ref_id),
                    edge_type=consts.PDG_EDGE
                )
            # Global variable
            elif in_var in self.global_var_names:
                self.global_var_reads.setdefault(in_var, []).append(ast_node_id)
        # "Out" variables
        for out_var in out_vars:
            # Try to update variable reference
            success = self._update_var_ref(out_var, ast_node_id)
            # Global variable
            if not success and out_var in self.global_var_names:
                self.global_var_writes.setdefault(out_var, []).append(ast_node_id)
    def _handle_decl_stmt(self, cpg, node_info, ast_node_id, depth):
        """ Handle declaration statement AST nodes. """
        # Variable or parameter declaration only
        decl_kind = node_info["decl_kind"]
        decl_kind_name = get_kind_name("decl_kind", decl_kind)
        # Parameter variable declarations
        if decl_kind_name=="Function":
            decls = node_info["param_decls"]
            scope_id = ast_node_id
        # Variable declarations
        elif decl_kind_name=="Var":
            decls = node_info["decls"]
            scope_id = self._scope_stack[-1]
        children = ast_children(cpg, ast_node_id)
        # Non-empty children
        non_empty_children = node_info["non_empty_children"]
        # Handle each declaration
        for i, (var_name, _) in enumerate(decls):
            if decl_kind_name=="Var" and i in non_empty_children:
                # Child index
                child_index = non_empty_children.index(i)
                # Build data dependency for initialization
                in_vars, out_vars = DataDependencyContext().analyze(cpg, children[child_index])
                # Add PDG edges to "in" variables
                self._add_pdg_edges(cpg, in_vars, out_vars, ast_node_id)
            # Create variable reference
            self._create_var_ref(var_name, scope_id, ast_node_id)
        return True
    def _handle_compound_stmt(self, cpg, node_info, ast_node_id, depth):
        """ Handle compound statement AST nodes. """
        # Push node ID into scope stack
        self._scope_stack.append(ast_node_id)
        return True
    def _handle_catch_stmt(self, cpg, node_info, ast_node_id, depth):
        """ Handle C++ catch statement AST nodes. """
        var_name = getattr(node_info, "name")
        # No variable declaration
        if var_name==None:
            return True
        # Create variable reference
        self._scope_stack.append(ast_node_id)
        self._create_var_ref(var_name, ast_node_id, ast_node_id)
        return True
    def _handle_stmt(self, cpg, node_info, ast_node_id, depth):
        """ Handle statement AST nodes. """
        # Do not handle non-leaf AST nodes
        ast_kind_name = get_kind_name("ast_kind", node_info["ast_kind"])
        if ast_kind_name in BRANCH_AST_KINDS:
            return True
        # Build data dependency for statement
        in_vars, out_vars = DataDependencyContext().analyze(cpg, ast_node_id)
        # Add PDG edges for statement
        self._add_pdg_edges(cpg, in_vars, out_vars, ast_node_id)
        return True
    def _on_exit(self, cpg, node_info, ast_node_id, depth):
        """ Callback when exiting an AST node. """
        # Nothing in stack or stack top doesn't match
        if not self._scope_stack or self._scope_stack[-1]!=ast_node_id:
            return
        # Pop node ID from scope stack
        self._scope_stack.pop()
        # Pop variable reference items
        for var_name, var_refs in self._var_refs_map.items():
            # Reference stack is empty
            if not var_refs:
                continue
            scope_id, ref_id = var_refs[-1]
            # Pop reference item
            if scope_id==ast_node_id:
                var_refs.pop()
    def construct_pdg(self, cpg):
        """ Construct PDG on existing AST-only CPG. """
        # Root scope
        self._scope_stack.append(cpg.graph["root"])
        # Traverse function AST and construct PDG
        traverse_ast(
            cpg,
            statement_only_visitor(ast_kinds_visitor({
                "CompoundStmt": self._handle_compound_stmt,
                "CXXCatchStmt": self._handle_catch_stmt,
                "DeclStmt": self._handle_decl_stmt
            }, self._handle_stmt)),
            self._on_exit
        )

class CallsCollector(object):
    """ Function calls collector. """
    def __init__(self, local_func_names, vul_rows=None, vul_ext_func_names=None):
        """ Initialize local function calls collector. """
        ## Local function names
        self.local_func_names = set(local_func_names)
        ## Vulnerable rows
        self.vul_rows = set(vul_rows) if vul_rows else None
        ## Vulnerable external function names
        self.vul_ext_func_names = set(vul_ext_func_names) if vul_ext_func_names else set()
        ## Vulnerable statements
        self.vul_node_ids = set()
        ## Vulnerable external function calls
        self.vul_ext_func_calls = {}
        ## Local function calls
        self.local_func_calls = {}
        ## Current statement ID
        self._current_stmt_id = None
        ## Callee ID stack
        self._callee_stack = []
    def _handle_call_expr(self, cpg, node_info, ast_node_id, depth):
        """ Handle function call expression AST nodes. """
        children = ast_children(cpg, ast_node_id)
        # Push callee ID into stack
        self._callee_stack.append(children[0])
        return True
    def _handle_decl_ref_expr(self, cpg, node_info, ast_node_id, depth):
        """ Handle declaration reference expression AST nodes. """
        current_stmt_id = self._current_stmt_id
        # Non-callee reference
        if not self._callee_stack:
            return True
        func_name = node_info["name"]
        # Local function
        if func_name in self.local_func_names:
            self.local_func_calls.setdefault(func_name, []).append(current_stmt_id)
        # External function
        else:
            # Vulnerable rows given
            if self.vul_rows:
                if current_stmt_id in self.vul_node_ids:
                    self.vul_ext_func_names.add(func_name)
            # Vulnerable external function calls given
            else:
                if func_name in self.vul_ext_func_names:
                    self.vul_ext_func_calls.setdefault(func_name, []).append(current_stmt_id)
        return True
    def _handle_stmt(self, cpg, node_info, ast_node_id, depth):
        """ Handle statement AST nodes. """
        # Do not handle non-leaf AST nodes
        ast_kind_name = get_kind_name("ast_kind", node_info["ast_kind"])
        if ast_kind_name in BRANCH_AST_KINDS:
            return True
        # Current statement ID
        self._current_stmt_id = ast_node_id
        # Vulnerable rows given
        vul_rows = self.vul_rows
        row = node_info["row"]
        if vul_rows!=None:
            # Check if current statement is in a vulnerable row
            if row in vul_rows:
                self.vul_node_ids.add(ast_node_id)
        # Traverse statement AST for further processing
        traverse_ast(
            cpg,
            ast_kinds_visitor({
                "CallExpr": self._handle_call_expr,
                "DeclRefExpr": self._handle_decl_ref_expr
            }),
            self._on_stmt_ast_exit,
            ast_node_id
        )
        return True
    def _on_stmt_ast_exit(self, cpg, node_info, ast_node_id, depth):
        """ Callback when exiting an AST node. """
        callee_stack = self._callee_stack
        # Try to pop callee ID from stack
        if callee_stack and callee_stack[-1]==ast_node_id:
            callee_stack.pop()
    def collect_calls(self, cpg):
        """ Collect local and vulnerable function calls. """
        # Traverse function AST and collect local function calls
        traverse_ast(
            cpg,
            statement_only_visitor(self._handle_stmt)
        )

def _pretty_print_cpg_callback(cpg, ast_node_id, depth, edge_types, successors):
    """ Pretty print graph callback function. """
    # Get graph neighbors
    neighbor_ids = cpg_neighbors(cpg, ast_node_id, edge_types, successors)
    # Print current node ID and neighbor IDs
    if neighbor_ids:
        print("{}: [{}]".format(
            ast_node_id, ", ".join((str(neighbor_id) for neighbor_id in neighbor_ids))
        ))
    # Traverse current node's neighbors
    return True

def pretty_print_cpg(cpg, edge_types):
    """ Pretty-print subgraph of a CPG. """
    for _, ast_node_id in cpg.nodes:
        # Get graph neighbors
        neighbor_ids = cpg_neighbors(cpg, ast_node_id, edge_types)
        # Print current node ID and neighbor IDs
        if neighbor_ids:
            print("{}: [{}]".format(
                ast_node_id, ", ".join((str(neighbor_id) for neighbor_id in neighbor_ids))
            ))

def pretty_print_pdg(cpg):
    """ Pretty-print the PDG subgraph of a CPG. """
    pretty_print_cpg(cpg, [consts.PDG_EDGE])
