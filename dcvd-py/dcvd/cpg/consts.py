# AST node type
AST_NODE = 0
# Auxiliary node type
AUX_NODE = 1

# AST edge type
AST_EDGE = 0
# PDG edge type
PDG_EDGE = 1
# Gadget link edge type
GADGET_LINK_EDGE = 2
# Feature graph edge type
FEAT_GRAPH_EDGE = 3

# Fixed AST raw feature
AST_RAW_FEATURE = 0
# Reference raw feature
REF_RAW_FEATURE = 1
# Type raw feature
TYPE_RAW_FEATURE = 2
# Literal raw feature
LITERAL_RAW_FEATURE = 3

# AST kind not found (Temporary)
TMP_AST_KIND_NOT_FOUND = 65535
