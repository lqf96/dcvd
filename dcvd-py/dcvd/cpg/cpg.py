import functools

from dcvd_cpp import get_kind_name
from . import consts

# Branch AST nodes
BRANCH_AST_KINDS = set([
    "CompoundStmt",
    "IfStmt",
    "SwitchStmt",
    "CaseStmt",
    "DefaultStmt",
    "ForStmt",
    "WhileStmt",
    "DoStmt",
    "CXXTryStmt",
    "CXXCatchStmt",
    # "FunctionDecl" only
    "DeclStmt"
])

def cpg_neighbors(cpg, ast_node_id, edge_types, successors=True):
    """ Get neighbors of an AST node in CPG. """
    edge_types = set(edge_types)
    # Node ID and neighbor accessor
    node_id = (consts.AST_NODE, ast_node_id)
    # Neighbors
    neighbors = []
    # Successors
    if successors:
        for neighbor_id in cpg.successors(node_id):
            # Do not include current node
            if node_id==neighbor_id:
                continue
            # Not desired edge type
            if cpg.edges[node_id, neighbor_id]["edge_type"] not in edge_types:
                continue
            neighbors.append(neighbor_id[1])
    # Predecessors
    else:
        for neighbor_id in cpg.predecessors(node_id):
            # Do not include current node
            if node_id==neighbor_id:
                continue
            # Not desired edge type
            if cpg.edges[neighbor_id, node_id]["edge_type"] not in edge_types:
                continue
            neighbors.append(neighbor_id[1])
    return neighbors

def ast_children(cpg, ast_node_id):
    """ Get AST children of an AST node. """
    return cpg_neighbors(cpg, ast_node_id, [consts.AST_EDGE])

def pdg_neighbors(cpg, ast_node_id, upstream=True):
    """ Get PDG neighbors of an AST node. """
    return cpg_neighbors(cpg, ast_node_id, [consts.PDG_EDGE], upstream)

def traverse_ast(cpg, on_enter, on_exit=None, ast_node_id=0, _depth=0):
    """ Traverse the AST of a CPG. """
    node_id = (consts.AST_NODE, ast_node_id)
    # Visit current node on enter
    node_info = cpg.nodes[node_id]
    traverse_children = on_enter(cpg, node_info, ast_node_id, _depth)
    # Traverse its AST children
    if traverse_children:
        for ast_child_id in cpg_neighbors(cpg, ast_node_id, [consts.AST_EDGE]):
            # Recursively visit child
            traverse_ast(cpg, on_enter, on_exit, ast_child_id, _depth+1)
    # Visit current node on exit
    if on_exit:
        on_exit(cpg, node_info, ast_node_id, _depth)

def traverse_pdg(cpg, on_enter, ast_node_id, upstream=True, on_exit=None):
    """ Traverse the PDG of a CPG. """
    traverse_cpg(
        cpg,
        on_enter,
        ast_node_id,
        [consts.PDG_EDGE],
        successors=upstream,
        on_exit=on_exit
    )

def traverse_cpg(cpg, on_enter, ast_node_id, edge_types, successors=True,
    on_exit=None, _visited_nodes=None):
    """ Traverse the CPG through given types of edges. """
    if _visited_nodes==None:
        _visited_nodes = set()
    # Graph node ID
    node_id = (consts.AST_NODE, ast_node_id)
    # Visit current node on enter
    node_info = cpg.nodes[node_id]
    traverse_neighbors = on_enter(cpg, node_info, ast_node_id)
    # Add to visited nodes
    _visited_nodes.add(ast_node_id)
    # Traverse its CPG neighbors
    if traverse_neighbors:
        for ast_neighbor_id in cpg_neighbors(cpg, ast_node_id, edge_types, successors):
            neighbor_id = (consts.AST_NODE, ast_neighbor_id)
            # Already visited
            if ast_neighbor_id in _visited_nodes:
                continue
            # Recursively visit neighbors
            traverse_cpg(
                cpg,
                on_enter,
                ast_neighbor_id,
                edge_types,
                successors,
                on_exit,
                _visited_nodes
            )
    # Visit current node on exit
    if on_exit:
        on_exit(cpg, node_info, ast_node_id)

def statement_only_visitor(on_enter):
    """ Create a statement-only AST visitor wrapper. """
    @functools.wraps(on_enter)
    def wrapper(cpg, node_info, ast_node_id, depth):
        """ Wrapper visitor function. """
        try:
            ast_kind = node_info["ast_kind"]
        except BaseException as e:
            print(node_info)
            raise e
        # Call original visitor
        traverse_children = on_enter(cpg, node_info, ast_node_id, depth)
        # Non-branch AST node, do not traverse its children
        ast_kind_name = get_kind_name("ast_kind", ast_kind)
        if ast_kind_name not in BRANCH_AST_KINDS:
            return False
        # Not function declaration
        if ast_kind_name=="DeclStmt":
            decl_kind_name = get_kind_name("decl_kind", node_info["decl_kind"])
            if decl_kind_name!="Function":
                return False
        return traverse_children
    return wrapper

def ast_kinds_visitor(kind_callback_map, fallback=None):
    """ Create kind-specific AST visitor wrapper. """
    def wrapper(cpg, node_info, ast_node_id, depth):
        """ Wrapper visitor function. """
        ast_kind = node_info["ast_kind"]
        ast_kind_name = get_kind_name("ast_kind", ast_kind) if ast_kind else None
        # Get kind callback
        kind_callback = kind_callback_map.get(ast_kind_name, fallback)
        if kind_callback:
            return kind_callback(cpg, node_info, ast_node_id, depth)
        else:
            return True
    return wrapper

class ASTCopier(object):
    """ AST copier class. """
    def __init__(self, node_id_transformer):
        """ Initialize AST copier. """
        ## Node ID transformer
        self.node_id_transformer = node_id_transformer
    def _on_enter(self, cpg, node_info, ast_node_id, depth):
        """ Callback when visiting an AST node. """
        # Copy current node
        target_node_id = (consts.AST_NODE, self.node_id_transformer(ast_node_id))
        self._target_cpg.add_node(target_node_id, **node_info)
        return True
    def _on_exit(self, cpg, node_info, ast_node_id, depth):
        """ Callback when exiting an AST node. """
        node_id_transformer = self.node_id_transformer
        target_node_id = (consts.AST_NODE, node_id_transformer(ast_node_id))
        # Get all AST children
        children = ast_children(cpg, ast_node_id)
        # Connect current node and its children in target CPG
        for ast_child_id in children:
            target_child_id = (consts.AST_NODE, node_id_transformer(ast_child_id))
            # Add edge in target CPG
            self._target_cpg.add_edge(
                target_node_id,
                target_child_id,
                edge_type=consts.AST_EDGE
            )
    def copy(self, source_cpg, ast_node_id, target_cpg):
        """ Copy AST from source CPG to destination CPG. """
        self._target_cpg = target_cpg
        # Traverse AST and copy nodes
        traverse_ast(source_cpg, self._on_enter, self._on_exit, ast_node_id)
        # Clear target CPG
        del self._target_cpg
