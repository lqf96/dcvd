import platform

from dcvd_cpp import get_kind_name, get_kind_repr
from .cpg import traverse_ast

# System include path
SYSTEM_INCLUDE_PATHS = {
    "Darwin": [
        # C/C++ headers
        "/usr/local/opt/llvm@7/include/c++/v1",
        "/usr/local/opt/llvm@7/lib/clang/7.0.0/include",
        # System headers
        "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include",
    ],
    "Linux": [
        # C/C++ headers
        "{env_root}/opt/llvm@7/include/c++/v1",
        "{env_root}/opt/llvm@7/lib/clang/7.0.0/include",
        # System headers
        "/usr/include"
    ]
}
# Kind properties
KIND_PROPS = set([
    "unary_op_kind",
    "binary_op_kind",
    "decl_kind"
])

def dcvd_cpp_args(source_files, source_ignore_paths=[], macro_defs=[], local_include_paths=[]):
    """ Generate arguments to be passed to AST generator. """
    # Platform-specific system include path
    sys_include_paths = SYSTEM_INCLUDE_PATHS[platform.system()]
    # Build arguments
    return [
        # Source files
        "-source-paths", ",".join(source_files),
        # System include paths
        "-sys-include-paths", ",".join(sys_include_paths),
        # Local include paths
        "-local-include-paths", ",".join(local_include_paths),
        # Macro definitions
        "-macro-defs", ",".join(macro_defs),
        # Source ignore paths
        "-src-ignore-paths", ",".join(source_ignore_paths),
        # Non-exist "source" location (silence LLVM CLI parser)
        "/ne"
    ]

def _pretty_print_ast_callback(cpg, node_info, ast_node_id, depth):
    """ Pretty print AST callback function. """
    # Indentation and node ID
    output = "  "*depth
    output += "({}) ".format(ast_node_id)
    # Copy node information
    node_info = dict(node_info)
    # Print AST kind
    ast_kind = node_info.pop("ast_kind")
    ast_kind_name = get_kind_name("ast_kind", ast_kind)
    output += ast_kind_name if ast_kind_name else "(Unknown)"
    # Print location
    row = node_info.pop("row")
    col = node_info.pop("col")
    output += " [loc: {}, {}]".format(row, col)
    # Print other information
    for prop, value in node_info.items():
        output += " [{}: {}".format(prop, value)
        # Get kind name and representation
        if prop in KIND_PROPS:
            kind_desc = []
            # Property kind name
            prop_kind_name = get_kind_name(prop, value)
            if prop_kind_name:
                kind_desc.append(prop_kind_name)
            # Property kind representation
            prop_kind_repr = get_kind_repr(prop, value)
            if prop_kind_repr:
                kind_desc.append("'{}'".format(prop_kind_repr))
            # Output
            output += " ("+", ".join(kind_desc)+")"
        output += "]"
    # Print output
    print(output)
    # Traverse current node's children
    return True

def pretty_print_ast(cpg, ast_node_id=0):
    """ Pretty print the AST in a CPG. """
    # Traverse AST and print lines
    traverse_ast(cpg, _pretty_print_ast_callback, ast_node_id=ast_node_id)
