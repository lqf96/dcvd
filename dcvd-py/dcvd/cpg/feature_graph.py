from networkx import DiGraph

from dcvd_cpp import get_kind_name, parse_testcase_source
from .ast import dcvd_cpp_args, traverse_ast
from .cpg import ASTCopier, pdg_neighbors, traverse_pdg, traverse_cpg, \
    ast_children, pdg_neighbors
from .pdg import PDGConstructor, CallsCollector, top_level_names
from . import consts

class SliceBuilder(object):
    """ Slice builder class. """
    def _node_id_transformer(self, ast_node_id):
        """ AST node ID transformer. """
        return (self._cpg_id, ast_node_id)
    def _on_visit_stmt(self, cpg, node_info, ast_node_id):
        """ Callback when visiting statement. """
        target_cpg = self._target_cpg
        # Stop copying current statement
        if self._stmt_count<=0:
            self._no_copy_set.add(ast_node_id)
            return False
        # Update statement count
        self._stmt_count -= 1
        # AST kind name
        ast_kind_name = get_kind_name("ast_kind", node_info["ast_kind"])
        # Declaration
        if ast_kind_name=="DeclStmt":
            decl_kind_name = get_kind_name("decl_kind", node_info["decl_kind"])
            # Function declaration
            if decl_kind_name=="Function":
                # Copy function declaration to target CPG
                node_id = (consts.AST_NODE, self._node_id_transformer(ast_node_id))
                target_cpg.add_node(node_id, **node_info)
                return True
        # Copy AST to target CPG
        copier = ASTCopier(self._node_id_transformer)
        copier.copy(cpg, ast_node_id, target_cpg)
        return True
    def _on_exit_stmt(self, cpg, node_info, ast_node_id):
        """ Callback when exiting from statement. """
        target_cpg = self._target_cpg
        # Node ID for target CPG
        node_id = (consts.AST_NODE, self._node_id_transformer(ast_node_id))
        # PDG neighbors
        neighbors = pdg_neighbors(cpg, ast_node_id, upstream=self._upstream)
        # Add edge between current node and PDG neighbors
        for ast_neighbor_id in neighbors:
            # Do not add edge if in no-copy set
            if ast_neighbor_id in self._no_copy_set:
                continue
            # Add edge to neighbor
            neighbor_id = (consts.AST_NODE, self._node_id_transformer(ast_neighbor_id))
            if self._upstream:
                target_cpg.add_edge(node_id, neighbor_id, edge_type=consts.PDG_EDGE)
            else:
                target_cpg.add_edge(neighbor_id, node_id, edge_type=consts.PDG_EDGE)
    def build_slice(self, source_cpg, cpg_id, target_cpg, entrypoint, upstream, stmt_limit):
        """ Build slice for CPG from given AST entrypoint. """
        ## CPG ID
        self._cpg_id = cpg_id
        ## Target CPG
        self._target_cpg = target_cpg
        ## Statements count
        self._stmt_count = stmt_limit
        ## Upstream flag
        self._upstream = upstream
        ## No-copy set
        self._no_copy_set = set()
        # Traverse PDG
        traverse_pdg(
            source_cpg,
            self._on_visit_stmt,
            entrypoint,
            upstream=upstream,
            on_exit=self._on_exit_stmt
        )
        # Number of statements in slice
        n_stmt = stmt_limit-self._stmt_count
        # Remove context variables
        del self._cpg_id
        del self._target_cpg
        del self._stmt_count
        del self._upstream
        del self._no_copy_set
        # Return whether function declaration is visited or not
        return n_stmt

class GadgetGraphBuilder(object):
    """ Gadget graph builder class. """
    def __init__(self, cpgs, local_func_names, global_var_names, pdg_constructor_map,
        calls_collector_map, call_rev_map):
        ## CPGs
        self._cpgs = cpgs
        ## Local function names
        self._local_func_names = local_func_names
        ## Global variable names
        self._global_var_names = global_var_names
        ## PDG constructor map
        self._pdg_constructor_map = pdg_constructor_map
        ## Calls collector map
        self._calls_collector_map = calls_collector_map
        ## Reverse local function call map
        self._call_rev_map = call_rev_map
    def _create_global_var_node(self, gadget_graph, var_cpg_id):
        """ Add global variable node to the gadget graph. """
        var_cpg = self._cpgs[var_cpg_id]
        var_cpg_root = var_cpg.graph["root"]
        # Source and target node ID
        source_id = (consts.AST_NODE, var_cpg_root)
        target_id = (consts.AST_NODE, (var_cpg_id, var_cpg_root))
        # Create node
        if target_id not in gadget_graph.nodes:
            gadget_graph.add_node(target_id, **var_cpg.nodes[source_id])
    def _connect_global_var_reads(self, gadget_graph, func_cpg_id):
        """ Connect global variable reads for function. """
        pdg_constructor = self._pdg_constructor_map[func_cpg_id]
        # Connect each variable
        for var_name, read_locs in pdg_constructor.global_var_reads.items():
            var_cpg_id = self._global_var_names[var_name]
            var_cpg = self._cpgs[var_cpg_id]
            var_cpg_root = var_cpg.graph["root"]
            # Create global variable node
            self._create_global_var_node(gadget_graph, var_cpg_id)
            # Connect each variable at each location
            for read_loc in read_locs:
                from_id = (consts.AST_NODE, (var_cpg_id, var_cpg_root))
                to_id = (consts.AST_NODE, (func_cpg_id, read_loc))
                # Add gadget link edge
                gadget_graph.add_edge(from_id, to_id, edge_type=consts.GADGET_LINK_EDGE)
    def _connect_global_var_writes(self, gadget_graph, func_cpg_id):
        """ Connect global variable writes for function. """
        pdg_constructor = self._pdg_constructor_map[func_cpg_id]
        # Connect each variable
        for var_name, write_locs in pdg_constructor.global_var_writes.items():
            var_cpg_id = self._global_var_names[var_name]
            var_cpg = self._cpgs[var_cpg_id]
            var_cpg_root = var_cpg.graph["root"]
            # Create global variable node
            self._create_global_var_node(gadget_graph, var_cpg_id)
            # Connect each variable at each location
            for write_loc in write_locs:
                from_id = (consts.AST_NODE, (func_cpg_id, write_loc))
                to_id = (consts.AST_NODE, (var_cpg_id, var_cpg_root))
                # Add gadget link edge
                gadget_graph.add_edge(from_id, to_id, edge_type=consts.GADGET_LINK_EDGE)
    def _get_func_calls_downstream(self, func_cpg_id, entrypoint):
        """ Get callees for current function after entrypoint. """
        calls_collector = self._calls_collector_map[func_cpg_id]
        result = {}
        # Filter call locations by entrypoint
        for func_name, call_locs in calls_collector.local_func_calls.items():
            filtered_call_locs = list(filter(
                lambda call_loc: call_loc>entrypoint,
                call_locs
            ))
            if filtered_call_locs:
                result[func_name] = filtered_call_locs
        return result
    def build_gadget_graph(self, cpg_id, entrypoint, upstream_limit, downstream_limit):
        """ Build gadget graph from given entrypoint. """
        # Global variable linking set
        global_var_linked_set = set()
        # Gadget graph and entrypoint
        gadget_graph = DiGraph(entrypoint=(cpg_id, entrypoint))
        # Build upstream part
        upstream_func_queue = [(cpg_id, entrypoint, None, None)]
        n_upstream_stmt = upstream_limit
        # Process each function in upstream queue until reaching limit
        while n_upstream_stmt>0 and upstream_func_queue:
            func_cpg_id, func_entrypoint, callee_cpg_id, call_locs = upstream_func_queue.pop()
            func_cpg = self._cpgs[func_cpg_id]
            func_cpg_root = func_cpg.graph["root"]
            func_name = func_cpg.nodes[(consts.AST_NODE, func_cpg_root)]["name"]
            # Append upstream slice to gadget graph
            n_slice = SliceBuilder().build_slice(
                source_cpg=func_cpg,
                cpg_id=func_cpg_id,
                target_cpg=gadget_graph,
                entrypoint=func_entrypoint,
                upstream=True,
                stmt_limit=n_upstream_stmt
            )
            n_upstream_stmt -= n_slice
            # Link global variables
            if func_cpg_id not in global_var_linked_set:
                # Link global variable reads
                self._connect_global_var_reads(gadget_graph, func_cpg_id)
                # Link global variable writes
                self._connect_global_var_writes(gadget_graph, func_cpg_id)
                global_var_linked_set.add(func_cpg_id)
            # Link callee position
            if callee_cpg_id!=None:
                callee_cpg = self._cpgs[callee_cpg_id]
                callee_cpg_root = callee_cpg.graph["root"]
                for call_loc in call_locs:
                    # Source and target ID
                    source_id = (consts.AST_NODE, (callee_cpg_id, callee_cpg_root))
                    target_id = (consts.AST_NODE, (func_cpg_id, call_loc))
                    # Add edge to gadget graph
                    gadget_graph.add_edge(source_id, target_id, edge_type=consts.GADGET_LINK_EDGE)
            # Get all function calls in the upstream
            call_locs_map = self._call_rev_map.get(func_name, {})
            for func_name, call_locs in call_locs_map.items():
                caller_cpg_id = self._local_func_names[func_name]
                caller_cpg = self._cpgs[caller_cpg_id]
                caller_cpg_root = caller_cpg.graph["root"]
                # Add to queue
                upstream_func_queue.append((
                    caller_cpg_id, max(call_locs), func_cpg_id, call_locs
                ))
        # Build downstream part
        downstream_func_queue = [(cpg_id, entrypoint, None, None)]
        n_downstream_stmt = downstream_limit
        # Process each function in downstream queue until reaching limit
        while n_downstream_stmt>0 and downstream_func_queue:
            func_cpg_id, func_entrypoint, caller_cpg_id, call_locs = downstream_func_queue.pop()
            func_cpg = self._cpgs[func_cpg_id]
            func_cpg_root = func_cpg.graph["root"]
            # Append downstream slice to gadget graph
            n_slice = SliceBuilder().build_slice(
                source_cpg=func_cpg,
                cpg_id=func_cpg_id,
                target_cpg=gadget_graph,
                entrypoint=func_entrypoint,
                upstream=False,
                stmt_limit=n_downstream_stmt
            )
            n_downstream_stmt -= n_slice
            # Link global variables
            if func_cpg_id not in global_var_linked_set:
                # Link global variable reads
                self._connect_global_var_reads(gadget_graph, func_cpg_id)
                # Link global variable writes
                self._connect_global_var_writes(gadget_graph, func_cpg_id)
                global_var_linked_set.add(func_cpg_id)
            # Link caller position
            if caller_cpg_id!=None:
                for call_loc in call_locs:
                    # Source and target ID
                    source_id = (consts.AST_NODE, (func_cpg_id, func_cpg_root))
                    target_id = (consts.AST_NODE, (caller_cpg_id, call_loc))
                    # Add edge to gadget graph
                    gadget_graph.add_edge(source_id, target_id, edge_type=consts.GADGET_LINK_EDGE)
            # Get all function calls in the downstream
            call_locs_map = self._get_func_calls_downstream(func_cpg_id, func_entrypoint)
            for func_name, call_locs in call_locs_map.items():
                callee_cpg_id = self._local_func_names[func_name]
                callee_cpg = self._cpgs[callee_cpg_id]
                callee_cpg_root = callee_cpg.graph["root"]
                # Add to queue
                downstream_func_queue.append((
                    callee_cpg_id, callee_cpg_root, func_cpg_id, call_locs
                ))
        return gadget_graph

# Cast AST kinds
CAST_AST_KINDS = set([
    "CStyleCastExpr",
    "CXXStaticCastExpr",
    "CXXDynamicCastExpr",
    "CXXConstCastExpr",
    "CXXReinterpretCastExpr"
])
# Literal AST kinds
LITERAL_AST_KINDS = set([
    "IntegerLiteral",
    "FloatingLiteral",
    "CharacterLiteral",
    "StringLiteral",
    "CXXBoolLiteralExpr"
])

class FeatureGraphBuilder(object):
    """ Feature graph builder class. """
    def __init__(self):
        """ Initialize feature graph builder. """
        ## Occurred raw tags
        self.raw_tags = set()
        ## Reverse tag mapping
        self.rev_tag_map = {}
    def _gen_raw_feature_ast(self, cpg, node_info, ast_node_id, depth):
        """ Generate raw features for each node during AST traversal. """
        # AST kind
        # TODO: No raw tag failure
        ast_kind = node_info.get("ast_kind", consts.TMP_AST_KIND_NOT_FOUND)
        ast_kind_name = get_kind_name("ast_kind", ast_kind) or ""
        if ast_kind==consts.TMP_AST_KIND_NOT_FOUND:
            print("Warning: AST kind for node {} not found in {}".format(
                ast_node_id, node_info
            ))
        # Cast expressions
        if ast_kind_name in CAST_AST_KINDS:
            raw_tag = (consts.TYPE_RAW_FEATURE, ast_kind, node_info["type"])
        # Literals
        elif ast_kind_name in LITERAL_AST_KINDS:
            raw_tag = (consts.LITERAL_RAW_FEATURE, ast_kind, node_info["lit_value"])
        # Declaration reference
        elif ast_kind_name=="DeclRefExpr":
            raw_tag = (consts.REF_RAW_FEATURE, node_info["name"], node_info["type"])
        # Unary operator
        elif ast_kind_name=="UnaryOperator":
            raw_tag = (consts.AST_RAW_FEATURE, ast_kind, node_info["unary_op_kind"])
        # Binary operator or compound assign operator
        elif ast_kind_name=="BinaryOperator" or ast_kind_name=="CompoundAssignOperator":
            raw_tag = (consts.AST_RAW_FEATURE, ast_kind, node_info["binary_op_kind"])
        # Other nodes
        else:
            raw_tag = (consts.AST_RAW_FEATURE, ast_kind)
        # Training mode
        if self._training:
            self.raw_tags.add(raw_tag)
        # Set raw tag
        node_info["raw_tag"] = raw_tag
        # Do not visit children for function declaration
        if ast_kind_name=="DeclStmt":
            decl_kind_name = get_kind_name("decl_kind", node_info["decl_kind"])
            if decl_kind_name=="Function":
                return False
        # Recursively visit children
        else:
            return True
    def _gen_raw_feature_cpg(self, cpg, node_info, ast_node_id):
        """ Generate raw features for each node during CPG traversal. """
        # Traverse AST subtree to build raw features
        traverse_ast(cpg, self._gen_raw_feature_ast, ast_node_id=ast_node_id)
        return True
    def _transform_raw_feature_ast(self, cpg, node_info, ast_node_id, depth):
        """ Transform raw features for each node during AST traversal. """
        rev_tag_map = self.rev_tag_map
        # Convert raw tag to tag
        # TODO: No raw tag failure
        if "raw_tag" not in node_info:
            print("Warning: Raw tag for node {} not found in {}".format(
                ast_node_id, node_info
            ))
        node_info["tag"] = rev_tag_map.get(node_info.get("raw_tag"), len(rev_tag_map)+3)
        # AST kind
        # TODO: Missing AST kind
        ast_kind = node_info.get("ast_kind", consts.TMP_AST_KIND_NOT_FOUND)
        ast_kind_name = get_kind_name("ast_kind", ast_kind) or ""
        if ast_kind==consts.TMP_AST_KIND_NOT_FOUND:
            print("Warning: AST kind for node {} not found in {}".format(
                ast_node_id, node_info
            ))
        # Add to visited node set
        self._visited_nodes.add((consts.AST_NODE, ast_node_id))
        # Do not visit children for function declaration
        if ast_kind_name=="DeclStmt":
            decl_kind_name = get_kind_name("decl_kind", node_info["decl_kind"])
            if decl_kind_name=="Function":
                return False
        # Recursively visit children
        else:
            return True
    def _transform_raw_feature_cpg(self, cpg, node_info, ast_node_id):
        """ Transform raw features for each node during CPG traversal. """
        # Traverse AST subtree to transform raw features
        traverse_ast(
            cpg,
            self._transform_raw_feature_ast,
            on_exit=self._convert_ast_edge,
            ast_node_id=ast_node_id
        )
        return True
    def _convert_ast_edge(self, cpg, node_info, ast_node_id, depth):
        """ Convert AST edge to auxiliary AST edge node. """
        node_id = (consts.AST_NODE, ast_node_id)
        # Get all AST children
        children = ast_children(cpg, ast_node_id)
        for ast_child_id in children:
            child_id = (consts.AST_NODE, ast_child_id)
            # Temporarily unlink current node and child node
            cpg.remove_edge(node_id, child_id)
            # Create a new node for AST edge
            ast_edge_node_id = (consts.AUX_NODE, (node_id, child_id))
            cpg.add_node(ast_edge_node_id, tag=0)
            self._visited_nodes.add(ast_edge_node_id)
            # Link current node, child node with AST edge node
            cpg.add_edge(node_id, ast_edge_node_id, edge_type=consts.FEAT_GRAPH_EDGE)
            cpg.add_edge(ast_edge_node_id, child_id, edge_type=consts.FEAT_GRAPH_EDGE)
    def _convert_pdg_edge(self, cpg, node_info, ast_node_id):
        """ Convert PDG edge to auxiliary PDG edge node. """
        node_id = (consts.AST_NODE, ast_node_id)
        # Get all PDG neighbors
        neighbors = pdg_neighbors(cpg, ast_node_id, upstream=self._upstream)
        for ast_neighbor_id in neighbors:
            neighbor_id = (consts.AST_NODE, ast_neighbor_id)
            # Edge tuple
            if self._upstream:
                edge_tuple = (node_id, neighbor_id)
            else:
                edge_tuple = (neighbor_id, node_id)
            # Get edge type
            edge_type = cpg.edges[edge_tuple]["edge_type"]
            # Temporarily unlink current node and neighbor node
            cpg.remove_edge(edge_tuple[0], edge_tuple[1])
            # Create a new node for PDG edge
            pdg_edge_node_id = (consts.AUX_NODE, edge_tuple)
            edge_node_tag = 1 if edge_type==consts.PDG_EDGE else 2
            cpg.add_node(pdg_edge_node_id, tag=edge_node_tag)
            self._visited_nodes.add(pdg_edge_node_id)
            # Link current node, child node with PDG edge node
            if self._upstream:
                cpg.add_edge(node_id, pdg_edge_node_id, edge_type=consts.FEAT_GRAPH_EDGE)
                cpg.add_edge(pdg_edge_node_id, neighbor_id, edge_type=consts.FEAT_GRAPH_EDGE)
            else:
                cpg.add_edge(neighbor_id, pdg_edge_node_id, edge_type=consts.FEAT_GRAPH_EDGE)
                cpg.add_edge(pdg_edge_node_id, node_id, edge_type=consts.FEAT_GRAPH_EDGE)
    def gen_raw_feature(self, cpg, entrypoint, training=False):
        """ Generate raw features for CPG. """
        self._training = training
        # Traverse CPG upstream to build raw features
        traverse_cpg(
            cpg,
            self._gen_raw_feature_cpg,
            entrypoint,
            [consts.PDG_EDGE, consts.GADGET_LINK_EDGE],
            successors=True
        )
        # Traverse CPG downstream to build raw features
        traverse_cpg(
            cpg,
            self._gen_raw_feature_cpg,
            entrypoint,
            [consts.PDG_EDGE, consts.GADGET_LINK_EDGE],
            successors=False
        )
    def build_rev_tag_map(self):
        """ Build reverse tag map. """
        # Hack: "0", "1" and "2" are for AST, PDG and gadget link edges
        self.rev_tag_map = dict((
            (raw_tag, i+3) for i, raw_tag in enumerate(sorted(self.raw_tags))
        ))
    def gen_feature_graph(self, cpg, entrypoint):
        """ Generate feature graphs for CPG. """
        # Set upstream flag to true
        self._upstream = True
        # Visited nodes
        visited_nodes = self._visited_nodes = set()
        # Copy CPG
        cpg = DiGraph(cpg)
        # Traverse CPG upstream to generate feature graph
        traverse_cpg(
            cpg,
            self._transform_raw_feature_cpg,
            entrypoint,
            [consts.PDG_EDGE, consts.GADGET_LINK_EDGE],
            successors=True,
            on_exit=self._convert_pdg_edge
        )
        # Set upstream flag to false
        self._upstream = False
        # Traverse CPG downstream to generate feature graph
        traverse_cpg(
            cpg,
            self._transform_raw_feature_cpg,
            entrypoint,
            [consts.PDG_EDGE, consts.GADGET_LINK_EDGE],
            successors=False,
            on_exit=self._convert_pdg_edge
        )
        # Get subgraph of visited nodes
        cpg = cpg.subgraph(visited_nodes)
        # Clear variables
        del self._upstream
        del self._visited_nodes
        # Return relabeled graph
        return cpg

def gadget_graphs_from_source(testcase_sources, macro_defs, source_ignore_paths,
    local_include_paths, vul_func_names, upstream_limit, downstream_limit):
    """ Generate gadget graphs from testcase source code. """
    # Prepare CLI arguments
    cli_args = dcvd_cpp_args(
        source_files=testcase_sources,
        source_ignore_paths=source_ignore_paths,
        local_include_paths=local_include_paths,
        macro_defs=macro_defs
    )
    # Parse AST and get top-level names
    cpgs = parse_testcase_source(cli_args)
    if not cpgs:
        return None
    local_func_names, global_var_names = top_level_names(cpgs)
    # Local function calls reverse mapping
    local_call_rev_map = {}
    # Build PDG for each function
    pdg_constructor_map = {}
    calls_collector_map = {}
    for func_name, i in local_func_names.items():
        func_cpg = cpgs[i]
        # Build PDG and append PDG constructor
        pdg_constructor = PDGConstructor(global_var_names)
        pdg_constructor.construct_pdg(func_cpg)
        pdg_constructor_map[i] = pdg_constructor
        # Collect calls and append calls collector
        calls_collector = CallsCollector(
            local_func_names,
            vul_ext_func_names=vul_func_names
        )
        calls_collector.collect_calls(func_cpg)
        calls_collector_map[i] = calls_collector
        # Update local function calls reverse mapping
        for local_func_name, node_ids in calls_collector.local_func_calls.items():
            call_rev_map = local_call_rev_map.setdefault(local_func_name, {})
            call_rev_map[func_name] = node_ids
    # Gadget graphs
    gadget_graphs = []
    # Gadget builder
    gadget_builder = GadgetGraphBuilder(
        cpgs=cpgs,
        local_func_names=local_func_names,
        global_var_names=global_var_names,
        pdg_constructor_map=pdg_constructor_map,
        calls_collector_map=calls_collector_map,
        call_rev_map=local_call_rev_map
    )
    for func_name, i in local_func_names.items():
        func_cpg = cpgs[i]
        # Get calls collector for CPG
        calls_collector = calls_collector_map[i]
        # Generate feature graph for each vulnerable node ID
        for call_locs in calls_collector.vul_ext_func_calls.values():
            for call_loc in call_locs:
                # Build gadget graph from call location
                gadget_graph = gadget_builder.build_gadget_graph(
                    cpg_id=i,
                    entrypoint=call_loc,
                    upstream_limit=upstream_limit,
                    downstream_limit=downstream_limit
                )
                gadget_graphs.append(gadget_graph)
    return gadget_graphs
