def write_feature_graphs(f, cpgs, labels):
    """ Write feature graphs to file. """
    # Write number graphs
    f.write("{}\n".format(len(cpgs)))
    # For each graph
    for cpg, label in zip(cpgs, labels):
        # Write number of nodes and label
        f.write("{} {}\n".format(len(cpg), label))
        # For each node
        for i, node_info in cpg.nodes.items():
            # Tag of the node
            line_data = [node_info["tag"]]
            # Number of neighbors and neighbors
            neighbors = sorted(cpg.successors(i))
            line_data.append(len(neighbors))
            line_data += neighbors
            # Extra node features
            extra_features = node_info.get("features")
            if extra_features is not None:
                line_data += extra_features
            # Write line data
            f.write(" ".join(str(item) for item in line_data)+"\n")
