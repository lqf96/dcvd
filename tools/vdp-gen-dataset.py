#! /usr/bin/env python3
import os, sys, json
import numpy as np
from networkx import convert_node_labels_to_integers

from dcvd.cpg.feature_graph import FeatureGraphBuilder, gadget_graphs_from_source
from dcvd.manifest import parse_testcase_manifest
from dcvd.legacy.dgcnn_io import write_feature_graphs

def tvt_indexes(n, ratio_train=0.6, ratio_validate=0.2):
    """ Generate indexes for training, validation and testing set. """
    idx = np.random.permutation(n)
    # Size of sets
    train_size = int(len(idx)*ratio_train)
    validate_size = int(len(idx)*ratio_validate)
    # Indexes
    train_idx = idx[:train_size]
    validate_idx = idx[train_size:train_size+validate_size]
    test_idx = idx[train_size+validate_size:]
    return train_idx, validate_idx, test_idx

def gen_vdp_dataset(sard_testcases_root, manifest_path, vdp_testcase_ids,
    vdp_vul_func_names, testcases_limit=np.inf):
    """ Generate feature graphs using VulDeePecker testcase IDs. """
    for cwe_id in vdp_testcase_ids.keys():
        vdp_testcase_ids[cwe_id] = set(vdp_testcase_ids[cwe_id])
    # CWE reverse map
    cwe_rev_map = dict((cwe_id, i+1) for i, cwe_id in enumerate(vdp_testcase_ids.keys()))
    # Gadget graphs and labels and feature graph builder
    gadget_graphs = []
    labels = []
    # Feature graph builder
    builder = FeatureGraphBuilder()
    # SARD source ignore paths
    sard_ignore_paths = [
        os.path.abspath(os.path.join(sard_testcases_root, "shared"))
    ]
    # Testcases counter
    testcases_counter = 0
    # Parse each testcase information
    for testcase_info in parse_testcase_manifest(manifest_path):
        testcase_id = testcase_info["id"]
        testcase_cwe_id = None
        testcase_category = None
        # Get CWE type
        for cwe_id, testcases in vdp_testcase_ids.items():
            if testcase_id in testcases:
                testcase_cwe_id = cwe_id
                testcase_category = cwe_rev_map[cwe_id]
                break
        # Not in VulDeePecker dataset
        if testcase_category==None:
            continue
        # Counter
        if testcases_counter>=testcases_limit:
            break
        testcases_counter += 1
        print("Processing testcase {} ({}, {}) ({})".format(
            testcase_id, testcase_cwe_id, testcase_category, testcases_counter
        ))
        # Local include paths
        local_include_paths = []
        testsuite_id = testcase_info["testsuite_id"]
        if testsuite_id:
            local_include_paths.append(
                os.path.abspath(os.path.join(sard_testcases_root, "shared", str(testsuite_id)))
            )
        # Testcase sources
        testcase_sources = []
        for source_info in testcase_info["source_files"]:
            file_path = source_info["path"]
            # C/C++ source file
            if file_path.endswith((".c", ".cpp")):
                file_path = os.path.abspath(os.path.join(sard_testcases_root, file_path))
                testcase_sources.append(file_path)
        # Mixed testcase
        if testcase_info["flaw_type"]=="mixed":
            # Generate good gadget graphs
            good_graphs = gadget_graphs_from_source(
                testcase_sources=testcase_sources,
                macro_defs=["OMITBAD"],
                source_ignore_paths=sard_ignore_paths,
                local_include_paths=local_include_paths,
                vul_func_names=vdp_vul_func_names[testcase_cwe_id],
                upstream_limit=40,
                downstream_limit=40
            )
            if good_graphs==None:
                print("Mixed (good) gadget graphs generation failed...")
                continue
            else:
                print("{} gadget graphs generated for mixed (good) testcase.".format(
                    len(good_graphs)
                ))
            good_labels = [0]*len(good_graphs)
            # Generate bad gadget graphs
            bad_graphs = gadget_graphs_from_source(
                testcase_sources=testcase_sources,
                macro_defs=["OMITGOOD"],
                source_ignore_paths=sard_ignore_paths,
                local_include_paths=local_include_paths,
                vul_func_names=vdp_vul_func_names[testcase_cwe_id],
                upstream_limit=40,
                downstream_limit=40
            )
            if bad_graphs==None:
                print("Mixed (bad) gadget graphs generation failed...")
                continue
            else:
                print("{} gadget graphs generated for mixed (bad) testcase.".format(
                    len(bad_graphs)
                ))
            bad_labels = [testcase_category]*len(bad_graphs)
            # Append to gadget graphs and labels
            gadget_graphs += good_graphs+bad_graphs
            labels += good_labels+bad_labels
        # Good or bad testcase
        else:
            is_bad = testcase_info["flaw_type"]=="flaw"
            # Generate gadget graphs
            testcase_gadget_graphs = gadget_graphs_from_source(
                testcase_sources=testcase_sources,
                macro_defs=[],
                source_ignore_paths=sard_ignore_paths,
                local_include_paths=local_include_paths,
                vul_func_names=vdp_vul_func_names[testcase_cwe_id],
                upstream_limit=40,
                downstream_limit=40
            )
            if testcase_gadget_graphs==None:
                print("{} gadget graphs generation failed...".format(
                    "Bad" if is_bad else "Good"
                ))
                continue
            else:
                print("{} gadget graphs generated for {} testcase.".format(
                    len(testcase_gadget_graphs), "bad" if is_bad else "good"
                ))
            # Labels
            testcase_labels = [
                testcase_category if is_bad else 0
            ]*len(testcase_gadget_graphs)
            # Append to gadget graphs and labels
            gadget_graphs += testcase_gadget_graphs
            labels += testcase_labels
        # Show number of gadget graphs generated
        print("Number of gadget graphs and labels: {}, {}".format(
            len(gadget_graphs), len(labels)
        ))
    # Divide into training, validation and testing set
    train_idx, vali_idx, test_idx = tvt_indexes(len(gadget_graphs))
    n_train, n_vali, n_test = len(train_idx), len(vali_idx), len(test_idx)
    # Build raw features
    for i, idx in enumerate(train_idx):
        gadget_graph = gadget_graphs[idx]
        print("Generating raw features for #{} ({}/{}) (Training)".format(
            idx, i+1, n_train
        ))
        builder.gen_raw_feature(
            gadget_graph,
            gadget_graph.graph["entrypoint"],
            training=True
        )
    # Build reverse tag map
    builder.build_rev_tag_map()
    vali_test_idx = np.concatenate((vali_idx, test_idx))
    for i, idx in enumerate(vali_test_idx):
        print("Generating raw features for #{} ({}/{}) (Validation / Testing)".format(
            idx, i+1, n_vali+n_test
        ))
        gadget_graph = gadget_graphs[idx]
        builder.gen_raw_feature(
            gadget_graph,
            gadget_graph.graph["entrypoint"]
        )
    # Generate feature graphs
    feature_graphs = []
    reordered_idx = np.concatenate((train_idx, vali_test_idx))
    for i, idx in enumerate(reordered_idx):
        print("Building feature graphs for #{} ({}/{})".format(
            idx, i+1, len(gadget_graphs)
        ))
        gadget_graph = gadget_graphs[idx]
        # Generate feature graph and convert to integer labels
        feature_graph = builder.gen_feature_graph(
            gadget_graph,
            gadget_graph.graph["entrypoint"]
        )
        feature_graph = convert_node_labels_to_integers(feature_graph)
        # Add feature graph
        feature_graphs.append(feature_graph)
    # Reorder labels
    labels = np.array(labels)[reordered_idx]
    print("Not vulnerable: {}".format((labels==0).sum()))
    for cwe_id, testcase_category in cwe_rev_map.items():
        print("{} ({}): {}".format(
            cwe_id, testcase_category, (labels==testcase_category).sum()
        ))
    # Return generated feature graphs
    return feature_graphs, labels, n_train, n_vali, n_test

def main(sard_root, vdp_testcase_ids_file, vdp_vul_func_names_file, output_file):
    """ VulDeePecker dataset generator program entry. """
    # VulDeePecker testcase IDs
    with open(vdp_testcase_ids_file) as f:
        vdp_testcase_ids = json.load(f)
    # VulDeePecker vulnerable function names file
    with open(vdp_vul_func_names_file) as f:
        vdp_vul_func_names = json.load(f)
    # SARD testcase root and manifest path
    sard_testcases_root = os.path.join(sard_root, "testcases")
    manifest_path = os.path.join(sard_root, "full_manifest.xml")
    # Generate dataset
    feature_graphs, labels, n_train, n_vali, n_test = gen_vdp_dataset(
        sard_testcases_root=sard_testcases_root,
        manifest_path=manifest_path,
        vdp_testcase_ids=vdp_testcase_ids,
        vdp_vul_func_names=vdp_vul_func_names
    )
    print("Train graphs: {}, Validation graphs: {}, Test graphs: {}".format(
        n_train, n_vali, n_test
    ))
    # Write feature graphs and labels
    with open(output_file, "w") as f:
        write_feature_graphs(f, feature_graphs, labels)
    return 0

if __name__=="__main__":
    # Show usage
    if len(sys.argv)<5:
        USAGE = "Usage: {} [SARD Root] [VDP Testcase IDs] [VDP Vulnerable Function Names] [Output]"
        print(USAGE.format(sys.argv[0]), file=sys.stderr)
        exit(1)
    # Call main function
    exit(main(*sys.argv[1:]))
