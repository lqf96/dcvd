#! /usr/bin/env python3
import os, sys, json

def main(vdp_root, output_path):
    """ Get SARD testcase IDs from VulDeePecker dataset. """
    cwe_testcases_map = {}
    for file_entry_a in os.scandir(vdp_root):
        # Not a CWE directory
        file_name = file_entry_a.name
        if not file_name.startswith("CWE-"):
            continue
        testcases = []
        # Collect testcases
        for file_entry_b in os.scandir(os.path.join(file_entry_a.path, "source_files")):
            test_case_name = file_entry_b.name
            # Add SARD testcase
            try:
                testcases.append(int(test_case_name))
            except:
                pass
        # Sort all testcases
        testcases.sort()
        # Add to CWE-testcases map
        cwe_testcases_map[file_name] = testcases
    # Write data to output path
    with open(output_path, "w") as f:
        json.dump(cwe_testcases_map, f)
    return 0

if __name__=="__main__":
    # Show usage
    if len(sys.argv)<3:
        print("Usage: {} [VDP Root] [Output Path]".format(sys.argv[0]), file=sys.stderr)
        exit(1)
    # Get testcase IDs
    exit(main(*sys.argv[1:]))
