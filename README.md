# Deep Code Vulnerability Detector
This is the code for "Deep Code Vulnerability Detector", the course project for CSE 546.

## Installation
* Ensure that `boost`, `boost-python` and `cmake` are installed. Ensure that `clang+llvm` are installed with RTTI enabled (otherwise there will be compilation errors).
* `cd` into `dcvd-cpp` directory, then run `./build.sh` to build the DCVD C/C++ parser.
* Create and activate a virtual environment for the project, `cd` into `dcvd-py` directory and run `./setup.py install` or `./setup.py develop` to install all Python dependencies for the project.
* `cd` into tools directory and now you are ready to generate feature graphs!

## Usage
* `vdp-testcases.py`: Collect all VulDeePecker NIST SARD code sample IDs from the VulDeePecker dataset. Usage: `./vdp-testcases.py [VulDeePecker Dataset Root] [Output Path]`.
* `vdp-gen-dataset.py`: Generate feature graphs from NIST SARD dataset. Usage: `./vdp-gen-dataset.py [SARD Root] [VDP Testcase IDs] [VDP Vulnerable Function Names] [Output]`.

## License
[MIT License](LICENSE)
