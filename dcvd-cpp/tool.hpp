#pragma once

#include <memory>
#include <string>
#include <vector>

#include <boost/python.hpp>
#include <clang/Frontend/FrontendAction.h>
#include <clang/Lex/PreprocessorOptions.h>
#include <clang/Tooling/Tooling.h>

#include "util.hpp"
#include "ast.hpp"

DCVD_NS(tool)
using std::shared_ptr;
using std::string;
using std::unique_ptr;
using std::vector;

namespace bp = boost::python;
using llvm::StringRef;
using clang::ASTConsumer;
using clang::ASTFrontendAction;
using clang::CompilerInstance;
using clang::CompilerInvocation;
using clang::DiagnosticConsumer;
using clang::FileManager;
using clang::FrontendAction;
using clang::PCHContainerOperations;
using clang::tooling::FrontendActionFactory;

using ast::CXXASTConsumer;

// DCVD C/C++ frontend action
class CXXFrontendAction : public ASTFrontendAction {
    /// Code Property Graphs (CPG) for testcase
    bp::list& cpgs;
    /// Source ignore paths
    vector<string>& source_ignore_paths;
public:
    // Frontend action constructor
    CXXFrontendAction(bp::list& _cpgs, vector<string>& _source_ignore_paths)
        : cpgs(_cpgs), source_ignore_paths(_source_ignore_paths) {}
    
    // Create a new AST consumer
    unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance& ci, StringRef source_path);
};

// DCVD C/C++ frontend action factory
class CXXFrontendActionFactory : public FrontendActionFactory {
    /// Code Property Graphs (CPG) for testcase
    bp::list& cpgs;
    /// Source ignore paths
    vector<string> source_ignore_paths;

    /// System include paths
    vector<string> system_include_paths;
    /// Local include paths
    vector<string> local_include_paths;
    /// Macro definitions
    vector<string> macro_defs;
public:
    // Factory constructor
    CXXFrontendActionFactory(
        bp::list& _cpgs,
        vector<string> _source_ignore_paths,
        vector<string> _system_include_paths,
        vector<string> _local_include_paths,
        vector<string> _macro_defs
    ) : cpgs(_cpgs),
        source_ignore_paths(_source_ignore_paths),
        system_include_paths(_system_include_paths),
        local_include_paths(_local_include_paths),
        macro_defs(_macro_defs) {}
 
    // Invokes the compiler with a FrontendAction created by create()
    bool runInvocation(
        shared_ptr<CompilerInvocation> invocation,
        FileManager* files,
        shared_ptr<PCHContainerOperations> pch_container_ops,
        DiagnosticConsumer* diag_consumer
    );

    // Create a new frontend action
    FrontendAction* create();
};
DCVD_END_NS
