#! /bin/sh

cd $(dirname $0)
# Run CMake in "build" directory
mkdir -p build
cd build
cmake ..
# Make Python module
make
