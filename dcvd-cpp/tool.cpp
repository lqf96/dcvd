#include <system_error>

#include <boost/algorithm/string.hpp>
#include <llvm/Support/CommandLine.h>
#include <llvm/Support/raw_ostream.h>
#include <clang/Lex/PreprocessorOptions.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include <clang/Tooling/Tooling.h>

#include "ast.hpp"
#include "tool.hpp"

DCVD_NS(tool)
using std::make_unique;
using std::error_code;

using llvm::errs;
using llvm::outs;
using llvm::raw_fd_ostream;
using llvm::cl::OptionCategory;
using llvm::cl::opt;
using llvm::cl::cat;
using clang::frontend::IncludeDirGroup;
using clang::tooling::CommonOptionsParser;
using clang::tooling::ClangTool;

// DCVD C/C++ parser program name
const char dcvd_program_name[] = "dcvd-parser-cpp";

// DCVD C/C++ parser option category
OptionCategory dcvd_option_category("DCVD C/C++");
// Source file paths
opt<string> source_paths_opt("source-paths", cat(dcvd_option_category));
// System include paths
opt<string> system_include_paths_opt("sys-include-paths", cat(dcvd_option_category));
// Local include paths
opt<string> local_include_paths_opt("local-include-paths", cat(dcvd_option_category));
// Macro definitions
opt<string> macro_defs_opt("macro-defs", cat(dcvd_option_category));
// Source ignore paths
opt<string> source_ignore_paths_opt("src-ignore-paths", cat(dcvd_option_category));

// Create a new AST consumer
unique_ptr<ASTConsumer> CXXFrontendAction::CreateASTConsumer(
    CompilerInstance& ci,
    StringRef source_path
) {
    return make_unique<CXXASTConsumer>(
        ci, source_path.str(), this->cpgs, this->source_ignore_paths
    );
}

// Invokes the compiler with a FrontendAction created by create()
bool CXXFrontendActionFactory::runInvocation(
    shared_ptr<CompilerInvocation> invocation,
    FileManager* files,
    shared_ptr<PCHContainerOperations> pch_container_ops,
    DiagnosticConsumer* diag_consumer
) {
    // Add macro defitions
    auto preprocessor_opts = invocation->PreprocessorOpts;
    for (string& macro_def : this->macro_defs)
        preprocessor_opts->addMacroDef(macro_def);
    
    auto header_search_opts = invocation->HeaderSearchOpts;
    // Add system include paths
    for (string& include_path : this->system_include_paths)
        header_search_opts->AddPath(include_path, IncludeDirGroup::Angled, false, false);
    // Add local include paths
    for (string& include_path : this->local_include_paths)
        header_search_opts->AddPath(include_path, IncludeDirGroup::Quoted, false, false);
    
    // Call base class method
    return FrontendActionFactory::runInvocation(
        invocation, files, pch_container_ops, diag_consumer
    );
}

// Create a new frontend action
FrontendAction* CXXFrontendActionFactory::create() {
    return new CXXFrontendAction(this->cpgs, this->source_ignore_paths);
}

// Convert string arguments to "argv" format
vector<const char*> args_to_argv(const char* prog_name, vector<string>& args) {
    vector<const char*> argv;

    // Append program name
    argv.push_back(prog_name);
    // Append argument pointers
    for (string& arg : args)
        argv.push_back(arg.data());
    
    return argv;
}

// Parse comma-separated list arguments
vector<string> parse_list_arg(string input, string separator = ",") {
    vector<string> output;

    // Split input by separator if input is not empty
    if (!input.empty())
        boost::split(output, input, boost::is_any_of(separator));

    return output;
}

// Force stream to redirect to given output
error_code force_redirect_stream(raw_fd_ostream* stream, string output_file) {
    // Destroy existing stream
    stream->~raw_fd_ostream();

    error_code error;
    // Reconstruct new stream at the same location
    new (stream) raw_fd_ostream(output_file, error);

    return error;
}

// Parse source code of given testcase
bp::object parse_testcase_source(bp::list py_args) {
    vector<string> args;
    // Convert arguments to C++ vector
    int py_argc = bp::len(py_args);
    for (int i=0;i<py_argc;i++)
        args.push_back(bp::extract<string>(py_args[i]));
    // Prepare argument for LLVM parser
    vector<const char*> argv = args_to_argv(dcvd_program_name, args);
    int argc = argv.size();

    // Force redirect LLVM output to "/dev/null"
    auto llvm_outs = (raw_fd_ostream*)(&outs()), llvm_errs = (raw_fd_ostream*)(&errs());
    force_redirect_stream(llvm_outs, "/dev/null");
    force_redirect_stream(llvm_errs, "/dev/null");

    // Parse arguments
    CommonOptionsParser parser(argc, argv.data(), dcvd_option_category);
    // Source paths
    auto source_paths = parse_list_arg(source_paths_opt.getValue());
    // System include paths
    auto system_include_paths = parse_list_arg(system_include_paths_opt.getValue());
    // Local include paths
    auto local_include_paths = parse_list_arg(local_include_paths_opt.getValue());
    // Macro definitions
    auto macro_defs = parse_list_arg(macro_defs_opt.getValue());
    // Source ignore paths
    auto source_ignore_paths = parse_list_arg(source_ignore_paths_opt.getValue());

    // Create new Clang tool instance
    ClangTool tool(parser.getCompilations(), source_paths);
    // Code property graphs
    bp::list cpgs;

    // Create DCVD frontend action factory
    CXXFrontendActionFactory factory(
        cpgs,
        source_ignore_paths,
        system_include_paths,
        local_include_paths,
        macro_defs
    );
    // Run action
    int result = tool.run(&factory);
    // Error happened, discard parse results
    if (result!=0)
        return bp::object();
    else
        return cpgs;
}
DCVD_END_NS

// DCVD C/C++ parser Python module
BOOST_PYTHON_MODULE(dcvd_cpp) {
    namespace bp = boost::python;
    using namespace dcvd;

    // Initialize maps
    ast::init_lookup_maps();

    // Expose functions to Python
    bp::def("parse_testcase_source", tool::parse_testcase_source);
    bp::def("get_kind_name", ast::get_kind_name);
    bp::def("get_kind_repr", ast::get_kind_repr);
}
