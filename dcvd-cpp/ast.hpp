#pragma once

#include <string>
#include <vector>

#include <boost/python.hpp>
#include <clang/AST/ASTConsumer.h>
#include <clang/AST/DeclGroup.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/AST/Stmt.h>
#include <clang/Frontend/CompilerInstance.h>

#include "util.hpp"

DCVD_NS(ast)
using std::string;
using std::vector;

namespace bp = boost::python;
using clang::ASTConsumer;
using clang::CompilerInstance;
using clang::DeclGroupRef;
using clang::FunctionDecl;
using clang::InitListExpr;
using clang::RecursiveASTVisitor;
using clang::Stmt;
using clang::VarDecl;

// DCVD C/C++ AST consumer
class CXXASTConsumer : public ASTConsumer {
    /// Compiler instance
    CompilerInstance& ci;
    /// Source file path
    string source_path;
    /// Code Property Graphs (CPG) for testcase
    bp::list& cpgs;
    /// Source ignore paths
    vector<string>& source_ignore_paths;

    // Handle top-level variable declarations
    void handle_var_decl(VarDecl* var_decl);
public:
    // AST consumer constructor
    CXXASTConsumer(
        CompilerInstance& _ci,
        string _source_path,
        bp::list& _cpgs,
        vector<string>& _source_ignore_paths
    ) : ci(_ci),
        source_path(_source_path),
        cpgs(_cpgs),
        source_ignore_paths(_source_ignore_paths) {}

    // Handle top-level declarations
    bool HandleTopLevelDecl(DeclGroupRef decl_group);
};

// DCVD C/C++ recursive AST visitor
class CXXASTVisitor : public RecursiveASTVisitor<CXXASTVisitor> {
    /// Compiler instance
    CompilerInstance& ci;
    /// Source file path
    string source_path;

    /// Code Property Graph (CPG) for single function
    bp::object cpg;
    /// AST node stack
    vector<uint64_t> nodes_stack;
    /// Number of AST nodes in graph
    uint64_t n_ast_nodes;

    // Initialize Code Property Graph
    void init_cpg();
    // Create CPG AST node
    uint64_t create_ast_node();
    // Create CPG AST edge
    void create_ast_edge(uint64_t from, uint64_t to);
    // Set CPG global property
    void set_cpg_prop(string name, bp::object value);
    // Set CPG node property
    void set_node_prop(uint64_t node_id, string name, bp::object value);
    // Set CPG edge property
    void set_edge_prop(uint64_t from, uint64_t to, string name, bp::object value);
public:
    // AST visitor constructor
    CXXASTVisitor(
        CompilerInstance& _ci,
        string _source_path
    ) : ci(_ci),
        source_path(_source_path),
        n_ast_nodes(0) {}
    
    // Visit function declaration
    bool VisitFunctionDecl(FunctionDecl* func_decl);
    // Visit statement
    bool VisitStmt(Stmt* stmt);
    // Hook function called after traversing through statement or expression
    bool dataTraverseStmtPost(Stmt* stmt);
    // InitListExpr-specific traverse handler
    bool TraverseInitListExpr(InitListExpr* expr, DataRecursionQueue* queue = nullptr);

    // Visit top-level variable declaration
    bool visit_top_level_var_decl(VarDecl* var_decl);
    // Get CPG for current function
    bp::object get_cpg();
};

// Initialize look-up maps
void init_lookup_maps();

// Get kind name for enum value
bp::object get_kind_name(string kind, uint64_t kind_value);

// Get representation for enum value
bp::object get_kind_repr(string kind, uint64_t kind_value);
DCVD_END_NS