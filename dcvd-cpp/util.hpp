#pragma once

#include <string>

#include <boost/python.hpp>

// DCVD namespace begin and end
#define DCVD_NS(ns) namespace dcvd { namespace ns {
#define DCVD_END_NS } }
