#include <string>
#include <unordered_map>
#include <exception>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <clang/AST/Decl.h>

#include "ast.hpp"

DCVD_NS(ast)
using std::stoull;
using std::unordered_map;
using std::invalid_argument;

using clang::BinaryOperatorKind;
using clang::Decl;
using clang::NamespaceDecl;
using clang::ParmVarDecl;
using clang::UnaryExprOrTypeTrait;
using clang::UnaryOperatorKind;

// AST node type
const uint64_t AST_NODE = 0;

// AST edge type
const uint64_t AST_EDGE = 0;

// Get source location for statement
template <typename T>
void get_source_location(
    CompilerInstance& ci,
    T* stmt,
    string* file_path,
    uint64_t* row,
    uint64_t* col
) {
    // Get source location in string
    auto src_begin = stmt->getSourceRange().getBegin();
    string src_loc = src_begin.printToString(ci.getSourceManager());

    // Split location string by ":"
    vector<string> split_src_loc;
    boost::split(split_src_loc, src_loc, boost::is_any_of(":"));

    // File path, row and column number when parsing failed
    file_path->clear();
    *row = *col = 0;
    // Valid location format
    if (split_src_loc.size()==3) {
        *file_path = split_src_loc[0];
        // Try to parse row and column number
        try {
            *row = stoull(split_src_loc[1]);
            *col = stoull(split_src_loc[2]);
        } catch (invalid_argument e) {
            file_path->clear();
            *row = *col = 0;
        }
    }
}

// Handle top-level declarations
bool CXXASTConsumer::HandleTopLevelDecl(DeclGroupRef decl_group) {
    // Ignore source files in given path
    for (string& ignore_path : this->source_ignore_paths)
        if (boost::starts_with(this->source_path, ignore_path))
            return false;

    // Visit each declaration
    for (Decl* decl : decl_group) {
        // Create recursive AST visitor
        CXXASTVisitor ast_visitor(this->ci, this->source_path);
        
        switch (decl->getKind()) {
            // Function declaration
            case Decl::Function: {
                // Traverse function declaration
                ast_visitor.TraverseFunctionDecl((FunctionDecl*)decl);

                break;
            }
            // Variable declaration
            case Decl::Var: {
                // Traverse variable declaration
                ast_visitor.visit_top_level_var_decl((VarDecl*)decl);

                break;
            }
            // Namespace declaration
            case Decl::Namespace: {
                auto ns_decl = (NamespaceDecl*)decl;

                // Declarations inside namespace
                auto child_decl_iter = ns_decl->decls();
                vector<Decl*> ns_children_decls(child_decl_iter.begin(), child_decl_iter.end());
                // Convert to DeclGroup
                auto ns_children_decl_group = DeclGroupRef::Create(
                    this->ci.getASTContext(), ns_children_decls.data(), ns_children_decls.size()
                );
                // Recursive visit nested declarations
                this->HandleTopLevelDecl(ns_children_decl_group);

                break;
            }
            // Do nothing for other kinds
            default: {}
        }

        // Append non-empty CPG
        auto cpg = ast_visitor.get_cpg();
        if (cpg!=bp::object())
            this->cpgs.append(cpg);
    }

    return true;
}

// Initialize Code Property Graph
void CXXASTVisitor::init_cpg() {
    // NetworkX DiGraph class
    static bp::object DiGraph = bp::import("networkx").attr("DiGraph");

    // Create a new graph
    if (this->cpg==bp::object())
        this->cpg = DiGraph();
}

// Create CPG AST node
uint64_t CXXASTVisitor::create_ast_node() {
    // Get new AST node ID
    uint64_t new_node_id = this->n_ast_nodes;
    this->n_ast_nodes++;

    // Add node
    this->cpg.attr("add_node")(
        bp::make_tuple(AST_NODE, new_node_id)
    );
    // Return node ID
    return new_node_id;
}

// Create CPG AST edge
void CXXASTVisitor::create_ast_edge(uint64_t from, uint64_t to) {
    this->cpg.attr("add_edge")(
        bp::make_tuple(AST_NODE, from),
        bp::make_tuple(AST_NODE, to)
    );
}

// Set CPG global property
void CXXASTVisitor::set_cpg_prop(string name, bp::object value) {
    // Set property on the graph data object
    this->cpg.attr("graph").attr("__setitem__")(name, value);
}

// Set CPG node property
void CXXASTVisitor::set_node_prop(uint64_t node_id, string name, bp::object value) {
    // Node data object
    bp::object node_data = this->cpg.attr("nodes").attr("__getitem__")(
        bp::make_tuple(AST_NODE, node_id)
    );
    // Set property
    node_data.attr("__setitem__")(name, value);
}

// Set CPG edge property
void CXXASTVisitor::set_edge_prop(uint64_t from, uint64_t to, string name, bp::object value) {
    // Edge data object
    bp::object edge_data = this->cpg.attr("edges").attr("__getitem__")(bp::make_tuple(
        bp::make_tuple(AST_NODE, from),
        bp::make_tuple(AST_NODE, to)
    ));
    // Set property
    edge_data.attr("__setitem__")(name, value);
}

// Visit function declaration
bool CXXASTVisitor::VisitFunctionDecl(FunctionDecl* func_decl) {
    // Ignore definition-only declaration
    if (!func_decl->hasBody())
        return false;
    
    string file_path;
    uint64_t row, col;
    // Get source location
    get_source_location(this->ci, func_decl, &file_path, &row, &col);
    // Ignore if not in source code (e.g. in header files)
    if (file_path!=this->source_path)
        return false;
    
    // Initialize CPG
    this->init_cpg();
    // Set source path
    this->set_cpg_prop("source_path", bp::str(this->source_path));

    // Root node
    uint64_t root_id = this->create_ast_node();
    // Set root node ID
    this->set_cpg_prop("root", bp::long_(root_id));
    // Set root node type
    this->set_node_prop(root_id, "ast_kind", bp::long_(int64_t(Stmt::DeclStmtClass)));
    this->set_node_prop(root_id, "decl_kind", bp::long_(int64_t(Decl::Function)));
    // Function name
    string func_name = func_decl->getNameAsString();
    this->set_node_prop(root_id, "name", bp::str(func_name));
    // Function return type
    string return_type = func_decl->getReturnType().getAsString();
    this->set_node_prop(root_id, "return_type", bp::str(return_type));
    // Source location
    this->set_node_prop(root_id, "row", bp::long_(row));
    this->set_node_prop(root_id, "col", bp::long_(col));

    auto func_params = func_decl->parameters();
    // Declarations exposed to Python
    bp::list py_decls;
    // Non-empty children
    bp::list non_empty_children;

    uint64_t i = 0;
    // Traverse parameters
    for (ParmVarDecl* param_decl : func_params) {
        // Declaration name
        string decl_name = param_decl->getNameAsString();
        // Declaration type
        string decl_type = param_decl->getType().getAsString();
        // Add to declarations
        py_decls.append(bp::make_tuple(decl_name, decl_type));

        // Add to non-empty children
        if (param_decl->getInit())
            non_empty_children.append(bp::long_(i));
        // Update index
        i++;
    }
    // Set declarations
    this->set_node_prop(root_id, "param_decls", py_decls);
    // Set non-empty children
    this->set_node_prop(root_id, "non_empty_children", non_empty_children);

    // Add root node to node stack
    this->nodes_stack.push_back(root_id);

    return true;
}

// Hook function called after traversing through statement or expression
bool CXXASTVisitor::dataTraverseStmtPost(Stmt* stmt) {
    // Pop node from nodes stack
    this->nodes_stack.pop_back();

    return true;
}

// Visit statement
bool CXXASTVisitor::VisitStmt(Stmt* stmt) {
    // Nodes stack
    auto& nodes_stack = this->nodes_stack;

    // Create new node
    uint64_t node_id = this->create_ast_node();
    // Parent node ID
    uint64_t parent_id = nodes_stack.back();
    // Push current node ID into nodes stack
    nodes_stack.push_back(node_id);

    // Link new node and parent
    this->create_ast_edge(parent_id, node_id);
    // Set edge type
    this->set_edge_prop(parent_id, node_id, "edge_type", bp::long_(AST_EDGE));
    // Set node type
    this->set_node_prop(node_id, "ast_kind", bp::long_(int64_t(stmt->getStmtClass())));

    string file_path;
    uint64_t row, col;
    // Get source location
    get_source_location(this->ci, stmt, &file_path, &row, &col);
    // Set source location
    this->set_node_prop(node_id, "row", bp::long_(row));
    this->set_node_prop(node_id, "col", bp::long_(col));

    // Begin handling statement type
    #define DCVD_AST_BEGIN_STMT(stmt_class) \
        case Stmt::stmt_class##Class: { \
            auto concrete_stmt = (clang::stmt_class*)stmt;
    // End handling statement type
    #define DCVD_AST_END_STMT break; }

    // TODO: Kind-specific information
    switch (stmt->getStmtClass()) {
        // Declaration statement
        DCVD_AST_BEGIN_STMT(DeclStmt)
            auto decl_group = concrete_stmt->getDeclGroup();

            // Declarations exposed to Python
            bp::list py_decls;
            // Non-empty children
            bp::list non_empty_children;

            // Add declaration names and types
            uint64_t i = 0;
            for (auto decl : decl_group) {
                // Skip non-variable declarations
                if (decl->getKind()!=Decl::Var)
                    continue;
                auto var_decl = (VarDecl*)decl;

                // Declaration name
                string decl_name = var_decl->getNameAsString();
                // Declaration type
                string decl_type = var_decl->getType().getAsString();
                // Add to declarations
                py_decls.append(bp::make_tuple(decl_name, decl_type));

                // Add to non-empty children
                if (var_decl->getInit())
                    non_empty_children.append(bp::long_(i));
                // Update index
                i++;
            }

            // Set declaration kind
            this->set_node_prop(node_id, "decl_kind", bp::long_(int64_t(Decl::Var)));
            // Set declarations
            this->set_node_prop(node_id, "decls", py_decls);
            // Set non-empty children
            this->set_node_prop(node_id, "non_empty_children", non_empty_children);
        DCVD_AST_END_STMT
        // For statement
        DCVD_AST_BEGIN_STMT(ForStmt)
            // Non-empty children
            bp::list non_empty_children;

            // Non-empty initalization node
            if (concrete_stmt->getInit())
                non_empty_children.append(bp::long_(0));
            // Force-visit predicate node
            if (concrete_stmt->getCond())
                non_empty_children.append(bp::long_(1));
            // Force-visit update node
            if (concrete_stmt->getInc())
                non_empty_children.append(bp::long_(2));
            // Force-visit loop body
            if (concrete_stmt->getBody())
                non_empty_children.append(bp::long_(3));

            // Set non-empty children
            this->set_node_prop(node_id, "non_empty_children", non_empty_children);
        DCVD_AST_END_STMT
        // C++ catch statement
        DCVD_AST_BEGIN_STMT(CXXCatchStmt)
            auto decl = concrete_stmt->getExceptionDecl();

            if (decl) {
                // Declaration name
                string decl_name = decl->getNameAsString();
                this->set_node_prop(node_id, "name", bp::str(decl_name));
                // Declaration type
                string decl_type = decl->getType().getAsString();
                this->set_node_prop(node_id, "type", bp::str(decl_type));
            }
        DCVD_AST_END_STMT

        // Integer literal
        DCVD_AST_BEGIN_STMT(IntegerLiteral)
            // Literal value
            uint64_t lit_value = concrete_stmt->getValue().getLimitedValue();
            this->set_node_prop(node_id, "lit_value", bp::long_(lit_value));
        DCVD_AST_END_STMT
        // Float literal
        DCVD_AST_BEGIN_STMT(FloatingLiteral)
            // Literal value
            double lit_value = concrete_stmt->getValue().convertToDouble();
            this->set_node_prop(node_id, "lit_value", bp::object(lit_value));
        DCVD_AST_END_STMT
        // Character literal
        DCVD_AST_BEGIN_STMT(CharacterLiteral)
            // Literal value
            uint64_t lit_value = concrete_stmt->getValue();
            this->set_node_prop(node_id, "lit_value", bp::long_(lit_value));
        DCVD_AST_END_STMT
        // String literal
        DCVD_AST_BEGIN_STMT(StringLiteral)
            // Literal value
            string lit_value = concrete_stmt->getBytes().str();
            this->set_node_prop(node_id, "lit_value", bp::str(lit_value));
        DCVD_AST_END_STMT
        // C++ Boolean literal expression
        DCVD_AST_BEGIN_STMT(CXXBoolLiteralExpr)
            // Literal value
            bool lit_value = concrete_stmt->getValue();
            this->set_node_prop(node_id, "lit_value", bp::object(lit_value));
        DCVD_AST_END_STMT

        // Unary operator
        DCVD_AST_BEGIN_STMT(UnaryOperator)
            // Unary operator kind
            UnaryOperatorKind unary_op_kind = concrete_stmt->getOpcode();
            this->set_node_prop(node_id, "unary_op_kind", bp::long_(int64_t(unary_op_kind)));
        DCVD_AST_END_STMT
        // Binary operator
        DCVD_AST_BEGIN_STMT(BinaryOperator)
            // Binary operator kind
            BinaryOperatorKind binary_op_kind = concrete_stmt->getOpcode();
            this->set_node_prop(node_id, "binary_op_kind", bp::long_(int64_t(binary_op_kind)));
        DCVD_AST_END_STMT
        // Compound assign operator
        DCVD_AST_BEGIN_STMT(CompoundAssignOperator)
            // Binary operator type
            BinaryOperatorKind binary_op_kind = concrete_stmt->getOpcode();
            this->set_node_prop(node_id, "binary_op_kind", bp::long_(int64_t(binary_op_kind)));
        DCVD_AST_END_STMT

        // Declaration reference expression
        DCVD_AST_BEGIN_STMT(DeclRefExpr)
            // Reference name
            string ref_name = concrete_stmt->getNameInfo().getAsString();
            this->set_node_prop(node_id, "name", bp::str(ref_name));
            // Reference type
            string ref_type = concrete_stmt->getType().getAsString();
            this->set_node_prop(node_id, "type", bp::str(ref_type));
        DCVD_AST_END_STMT
        // Implicit cast expression
        DCVD_AST_BEGIN_STMT(ImplicitCastExpr)
            // Cast type
            string cast_type = concrete_stmt->getType().getAsString();
            this->set_node_prop(node_id, "type", bp::str(cast_type));
        DCVD_AST_END_STMT
        // Explicit cast expression
        DCVD_AST_BEGIN_STMT(CStyleCastExpr)
            // Cast type
            string cast_type = concrete_stmt->getType().getAsString();
            this->set_node_prop(node_id, "type", bp::str(cast_type));
        DCVD_AST_END_STMT
        // Member expression
        DCVD_AST_BEGIN_STMT(MemberExpr)
            // Member name
            string member_name = concrete_stmt->getMemberNameInfo().getAsString();
            this->set_node_prop(node_id, "name", bp::str(member_name));
            // Is indirect access
            bool is_indirect = concrete_stmt->isArrow();
            this->set_node_prop(node_id, "indirect", bp::object(is_indirect));
        DCVD_AST_END_STMT
        // Either type or (unevaluated) expression operand expression
        DCVD_AST_BEGIN_STMT(UnaryExprOrTypeTraitExpr)
            // UETT kind
            UnaryExprOrTypeTrait uett_kind = concrete_stmt->getKind();
            this->set_node_prop(node_id, "uett_kind", bp::long_(int64_t(uett_kind)));
        DCVD_AST_END_STMT
        // C++ static cast expression
        DCVD_AST_BEGIN_STMT(CXXStaticCastExpr)
            // Cast type
            string cast_type = concrete_stmt->getType().getAsString();
            this->set_node_prop(node_id, "type", bp::str(cast_type));
        DCVD_AST_END_STMT
        // C++ dynamic cast expression
        DCVD_AST_BEGIN_STMT(CXXDynamicCastExpr)
            // Cast type
            string cast_type = concrete_stmt->getType().getAsString();
            this->set_node_prop(node_id, "type", bp::str(cast_type));
        DCVD_AST_END_STMT
        // C++ constant cast expression
        DCVD_AST_BEGIN_STMT(CXXConstCastExpr)
            // Cast type
            string cast_type = concrete_stmt->getType().getAsString();
            this->set_node_prop(node_id, "type", bp::str(cast_type));
        DCVD_AST_END_STMT
        // C++ reinterpret cast expression
        DCVD_AST_BEGIN_STMT(CXXReinterpretCastExpr)
            // Cast type
            string cast_type = concrete_stmt->getType().getAsString();
            this->set_node_prop(node_id, "type", bp::str(cast_type));
        DCVD_AST_END_STMT
        
        // Do nothing for other statements
        default: {}
    }

    return true;
}

// Visit top-level variable declaration
bool CXXASTVisitor::visit_top_level_var_decl(VarDecl* var_decl) {
    string file_path;
    uint64_t row, col;
    // Get source location
    get_source_location(this->ci, var_decl, &file_path, &row, &col);
    // Ignore if not in source code (e.g. in header files)
    if (file_path!=this->source_path)
        return false;
    
    // Initialize CPG
    this->init_cpg();
    // Set source path
    this->set_cpg_prop("source_path", bp::str(this->source_path));

    // Root node
    uint64_t root_id = this->create_ast_node();
    // Set root node ID
    this->set_cpg_prop("root", bp::long_(root_id));
    // Set root node type
    this->set_node_prop(root_id, "ast_kind", bp::long_(int64_t(Stmt::DeclStmtClass)));
    this->set_node_prop(root_id, "decl_kind", bp::long_(int64_t(Decl::Var)));
    // Variable name
    string var_name = var_decl->getNameAsString();
    this->set_node_prop(root_id, "name", bp::str(var_name));
    // Variable type
    string var_type = var_decl->getType().getAsString();
    this->set_node_prop(root_id, "type", bp::str(var_type));
    // Set source location
    this->set_node_prop(root_id, "row", bp::long_(row));
    this->set_node_prop(root_id, "col", bp::long_(col));

    return true;
}

// InitListExpr-specific traverse handler
bool CXXASTVisitor::TraverseInitListExpr(
    InitListExpr* expr,
    DataRecursionQueue* queue
) {
    // Only traverse once over semantic form of expression
    return this->TraverseSynOrSemInitListExpr(
        expr->isSemanticForm()?expr:expr->getSemanticForm(),
        queue
    );
}

// Get CPG for current function
bp::object CXXASTVisitor::get_cpg() {
    return this->cpg;
}

// Enum value-kind name map
unordered_map<string, unordered_map<uint64_t, const char*>> value_name_map;
// Enum value-kind representation map
unordered_map<string, unordered_map<uint64_t, const char*>> value_repr_map;

// Initialize look-up maps
void init_lookup_maps() {
    #define X(stmt_class) {(uint64_t)Stmt::stmt_class##Class, #stmt_class},
    // Statement classes
    unordered_map<uint64_t, const char*> ast_kind_map = {
        #include "stmt.def"
    };
    value_name_map["ast_kind"] = ast_kind_map;
    #undef X

    #define X(decl_kind) {(uint64_t)Decl::decl_kind, #decl_kind},
    // Declaration kinds
    unordered_map<uint64_t, const char*> decl_kind_map = {
        #include "decl.def"
    };
    value_name_map["decl_kind"] = decl_kind_map;
    #undef X

    #define X(unary_op_kind, unary_op_repr) \
        unary_op_name_map[UnaryOperatorKind::unary_op_kind] = #unary_op_kind; \
        unary_op_repr_map[UnaryOperatorKind::unary_op_kind] = unary_op_repr;
    // Unary operator kinds
    unordered_map<uint64_t, const char*> unary_op_name_map;
    unordered_map<uint64_t, const char*> unary_op_repr_map;
    #include "unary_op.def"
    value_name_map["unary_op_kind"] = unary_op_name_map;
    value_repr_map["unary_op_kind"] = unary_op_repr_map;
    #undef X

    #define X(binary_op_kind, binary_op_repr) \
        binary_op_name_map[BinaryOperatorKind::binary_op_kind] = #binary_op_kind; \
        binary_op_repr_map[BinaryOperatorKind::binary_op_kind] = binary_op_repr;
    // Binary operator kinds
    unordered_map<uint64_t, const char*> binary_op_name_map;
    unordered_map<uint64_t, const char*> binary_op_repr_map;
    #include "binary_op.def"
    value_name_map["binary_op_kind"] = binary_op_name_map;
    value_repr_map["binary_op_kind"] = binary_op_repr_map;
    #undef X
}

// Get kind name for enum value
bp::object get_kind_name(string kind, uint64_t kind_value) {
    // Find map for given kind
    auto kind_map_iter = value_name_map.find(kind);
    if (kind_map_iter==value_name_map.cend())
        return bp::object();
    auto& kind_map = kind_map_iter->second;

    // Find kind value
    auto kind_name_iter = kind_map.find(kind_value);
    if (kind_name_iter==kind_map.cend())
        return bp::object();
    else
        return bp::str(kind_name_iter->second);
}

// Get representation for enum value
bp::object get_kind_repr(string kind, uint64_t kind_value) {
    // Find map for given kind
    auto kind_map_iter = value_repr_map.find(kind);
    if (kind_map_iter==value_name_map.cend())
        return bp::object();
    auto& kind_map = kind_map_iter->second;

    // Find kind value
    auto kind_repr_iter = kind_map.find(kind_value);
    if (kind_repr_iter==kind_map.cend())
        return bp::object();
    else
        return bp::str(kind_repr_iter->second);
}
DCVD_END_NS
